package com.shimmerresearch.android;

import android.app.Dialog;
import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.util.Log;
import android.util.Pair;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;


import com.shimmerresearch.bluetooth.ShimmerBluetooth;
import com.shimmerresearch.driver.FormatCluster;
import com.shimmerresearch.driver.ObjectCluster;
import com.shimmerresearch.tools.Logging;
import com.shimmerresearch.webSockets.WebSocket;
import com.shimmerresearch.webSockets.WebSocketAdapter;
import com.shimmerresearch.webSockets.WebSocketException;
import com.shimmerresearch.webSockets.WebSocketExtension;
import com.shimmerresearch.webSockets.WebSocketFactory;
import com.shimmerresearch.webSockets.WebSocketListener;
import com.shimmerresearch.webSockets.WebSocketState;

import org.apache.commons.lang3.ObjectUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import javax.vecmath.Tuple2d;

import static java.lang.Double.parseDouble;
import static java.lang.Thread.sleep;
import android.content.Intent;




/**
 * Created by mingli-student on 2/6/2018.
 */


public class ShimmerAdvance extends Shimmer implements ShimmerBluetooth.DataProcessing {

    //**********************************************************************************************
    //***** Class Variables and Constructors
    //**********************************************************************************************



    boolean mWebSocketStreaming = false;
    double mStartTime = 0;
    boolean mCloseSockets = false;
    String human_subject_type="DNE";



    //Experiment values for timing the streaming and reading lag
    /*
    int read_count = 1;
    long time_start;
    long time_end;
    */
    //Experiment values for timing the streaming and reading lag

    //bluetooth to logger
    public HashMap<String, Logging> mStoredFiles = new HashMap<>();
    /**
     * The timeout value in milliseconds for socket connection.
     */
    private static final int TIMEOUT = 50000;
    HashMap<Pair<String, String>, WebSocket> mStreamSockets = new HashMap<>();
    String serverMsg="init";
    //fresno state server
    private static final String SERVER = "wss://bsncloud.csufresno.edu/wss2";


    //Constructors from superclass Shimmer all imported

    /**
     * Constructor. Prepares a new Bluetooth session.
     *
     * @param context       The UI Activity Context
     * @param handler       A Handler to send messages back to the UI Activity
     * @param myname        To allow the user to set a unique identifier for each Shimmer device
     * @param countiousSync A boolean value defining whether received packets should be checked continuously for the correct start and end of packet.
     */
    public ShimmerAdvance(Handler handler, String myName, Boolean continousSync) {

        super(handler, myName, continousSync);
        setDataProcessing((DataProcessing) this);
    }

    public ShimmerAdvance(Context context, Handler handler, String myName, Boolean continousSync) {

        super(context, handler, myName, continousSync);
        setDataProcessing((DataProcessing) this);
    }

    /**
     * Constructor. Prepares a new Bluetooth session. Additional fields allows the device to be set up immediately.
     *
     * @param context           The UI Activity Context
     * @param handler           A Handler to send messages back to the UI Activity
     * @param myname            To allow the user to set a unique identifier for each Shimmer device
     * @param samplingRate      Defines the sampling rate
     * @param accelRange        Defines the Acceleration range. Valid range setting values for the Shimmer 2 are 0 (+/- 1.5g), 1 (+/- 2g), 2 (+/- 4g) and 3 (+/- 6g). Valid range setting values for the Shimmer 2r are 0 (+/- 1.5g) and 3 (+/- 6g).
     * @param gsrRange          Numeric value defining the desired gsr range. Valid range settings are 0 (10kOhm to 56kOhm),  1 (56kOhm to 220kOhm), 2 (220kOhm to 680kOhm), 3 (680kOhm to 4.7MOhm) and 4 (Auto Range).
     * @param setEnabledSensors Defines the sensors to be enabled (e.g. 'Shimmer.SENSOR_ACCEL|Shimmer.SENSOR_GYRO' enables the Accelerometer and Gyroscope)
     * @param countiousSync     A boolean value defining whether received packets should be checked continuously for the correct start and end of packet.
     */
    public ShimmerAdvance(Context context, Handler handler, String myName, double samplingRate, int accelRange, int gsrRange, long setEnabledSensors, boolean continousSync) {

        super(context, handler, myName, samplingRate, accelRange, gsrRange, setEnabledSensors, continousSync);
        setDataProcessing((DataProcessing) this);
    }

    /**
     * Constructor. Prepares a new Bluetooth session. Additional fields allows the device to be set up immediately.
     *
     * @param context           The UI Activity Context
     * @param handler           A Handler to send messages back to the UI Activity
     * @param myname            To allow the user to set a unique identifier for each Shimmer device
     * @param samplingRate      Defines the sampling rate
     * @param accelRange        Defines the Acceleration range. Valid range setting values for the Shimmer 2 are 0 (+/- 1.5g), 1 (+/- 2g), 2 (+/- 4g) and 3 (+/- 6g). Valid range setting values for the Shimmer 2r are 0 (+/- 1.5g) and 3 (+/- 6g).
     * @param gsrRange          Numeric value defining the desired gsr range. Valid range settings are 0 (10kOhm to 56kOhm),  1 (56kOhm to 220kOhm), 2 (220kOhm to 680kOhm), 3 (680kOhm to 4.7MOhm) and 4 (Auto Range).
     * @param setEnabledSensors Defines the sensors to be enabled (e.g. 'Shimmer.SENSOR_ACCEL|Shimmer.SENSOR_GYRO' enables the Accelerometer and Gyroscope)
     * @param countiousSync     A boolean value defining whether received packets should be checked continuously for the correct start and end of packet.
     */
    public ShimmerAdvance(Context context, Handler handler, String myName, double samplingRate, int accelRange, int gsrRange, long setEnabledSensors, boolean continousSync, int magGain) {

        super(context, handler, myName, samplingRate, accelRange, gsrRange, setEnabledSensors, continousSync, magGain);
        setDataProcessing((DataProcessing) this);
    }


    /**
     * Constructor. Prepares a new Bluetooth session. Additional fields allows the device to be set up immediately.
     *
     * @param context           The UI Activity Context
     * @param handler           A Handler to send messages back to the UI Activity
     * @param myname            To allow the user to set a unique identifier for each Shimmer device
     * @param samplingRate      Defines the sampling rate
     * @param accelRange        Defines the Acceleration range. Valid range setting values for the Shimmer 2 are 0 (+/- 1.5g), 1 (+/- 2g), 2 (+/- 4g) and 3 (+/- 6g). Valid range setting values for the Shimmer 2r are 0 (+/- 1.5g) and 3 (+/- 6g).
     * @param gsrRange          Numeric value defining the desired gsr range. Valid range settings are 0 (10kOhm to 56kOhm),  1 (56kOhm to 220kOhm), 2 (220kOhm to 680kOhm), 3 (680kOhm to 4.7MOhm) and 4 (Auto Range).
     * @param setEnabledSensors Defines the sensors to be enabled (e.g. 'Shimmer.SENSOR_ACCEL|Shimmer.SENSOR_GYRO' enables the Accelerometer and Gyroscope)
     * @param countiousSync     A boolean value defining whether received packets should be checked continuously for the correct start and end of packet.
     */
    public ShimmerAdvance(Context context, Handler handler, String myName, double samplingRate, int accelRange, int gsrRange, long setEnabledSensors, boolean continousSync, boolean enableLowPowerAccel, boolean enableLowPowerGyro, boolean enableLowPowerMag, int gyroRange, int magRange) {

        super(context, handler, myName, samplingRate, accelRange, gsrRange, setEnabledSensors, continousSync, enableLowPowerAccel, enableLowPowerGyro, enableLowPowerMag, gyroRange, magRange);
        setDataProcessing((DataProcessing) this);
    }


    /**
     * Constructor. Prepares a new Bluetooth session. Additional fields allows the device to be set up immediately.
     *
     * @param context           The UI Activity Context
     * @param handler           A Handler to send messages back to the UI Activity
     * @param myname            To allow the user to set a unique identifier for each Shimmer device
     * @param samplingRate      Defines the sampling rate
     * @param accelRange        Defines the Acceleration range. Valid range setting values for the Shimmer 2 are 0 (+/- 1.5g), 1 (+/- 2g), 2 (+/- 4g) and 3 (+/- 6g). Valid range setting values for the Shimmer 2r are 0 (+/- 1.5g) and 3 (+/- 6g).
     * @param gsrRange          Numeric value defining the desired gsr range. Valid range settings are 0 (10kOhm to 56kOhm),  1 (56kOhm to 220kOhm), 2 (220kOhm to 680kOhm), 3 (680kOhm to 4.7MOhm) and 4 (Auto Range).
     * @param setEnabledSensors Defines the sensors to be enabled (e.g. 'Shimmer.SENSOR_ACCEL|Shimmer.SENSOR_GYRO' enables the Accelerometer and Gyroscope)
     * @param countiousSync     A boolean value defining whether received packets should be checked continuously for the correct start and end of packet.
     */
    public ShimmerAdvance(Context context, Handler handler, String myName, double samplingRate, int accelRange, int gsrRange, long setEnabledSensors, boolean continousSync, boolean enableLowPowerAccel, boolean enableLowPowerGyro, boolean enableLowPowerMag, int gyroRange, int magRange, byte[] exg1, byte[] exg2) {

        super(context, handler, myName, samplingRate, accelRange, gsrRange, setEnabledSensors, continousSync, enableLowPowerAccel, enableLowPowerGyro, enableLowPowerMag, gyroRange, magRange, exg1, exg2);
        setDataProcessing((DataProcessing) this);
    }


    //Implementing Data Processing from ShimmerBluetooth Super Class called after the BuldMsg function has formatted the object cluster
    // Initialise Process Data here. This is called whenever the startStreaming command is called and can be used to initialise algorithms
    //initialize mArchiveLog in order to log data when the first object cluster is received
    @Override
    public void InitializeProcessData() {

        mStartTime = System.currentTimeMillis();
        //mArchiveLog = new Logging(fromMilisecToDate(System.currentTimeMillis()) + "Archive" + this.getBluetoothAddress(), ",", "TestShimmer");
        //mIsArcLog = true;
    }

    /**
     * Process data here, algorithms can access the object cluster built by the buildMsg method here
     *
     * @param ojc the objectCluster built by the buildMsg method
     * @return the processed objectCluster
     */
    @Override
    public ObjectCluster ProcessData(ObjectCluster ojc) {

        String[] sensorNames;
        String format;
        format = "CAL";
        double timeStamp = 0;

        List<String> connectedNotOpenList = new ArrayList<String>() ;

        /*
        //Timing Experiment ******************
        if (read_count == 0) {
            //ws.sendText("first_in_segment");
            time_start = System.currentTimeMillis();
            Log.d("Timing Experiment", "segment start");
        }
        if (read_count == 99) {
            //ws.sendText("last_in_segment");
            time_end = System.currentTimeMillis();
            read_count = -1;
            Log.d("Timing Experiment", "First: " + (time_end - time_start));
        }
        read_count++;
        */
        //ws.sendText("packet_start");
        if (mWebSocketStreaming) {
            ByteBuffer wrapped = ByteBuffer.wrap(ojc.mSystemTimeStamp);
            timeStamp = wrapped.getDouble();

            //Timing Experiment ******************
            /*
            if (read_count == 1) {
                ws.sendText("first_in_segment");
                time_start = System.currentTimeMillis();
                Log.d("Timing Experiment", "segment start");
            }
            if (read_count == 9999) {
                ws.sendText("last_in_segment");
                time_end = System.currentTimeMillis();
                read_count = 0;
                Log.d("Timing Experiment", "First: " + (time_end - time_start));
            }
            read_count++;
            ws.sendText("packet_start");
            */
            //Timing Experiment ******************

            //send the time for all samples in this object cluster

            /*if (mStreamSockets.containsKey(Pair.create(ojc.mBluetoothAddress, "Time"))) {
                if (mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, "Time"))).isOpen()) {
                    mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, "Time"))).sendText("Time");
                    mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, "Time"))).sendText(String.valueOf(timeStamp));
                    mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, "Time"))).sendText("milisec");
                }
                else {
                    //socket exists but is not open
                    connectedNotOpenList.add("Time");
                }
            } else { //create socket for time
                try {
                    mStreamSockets.put(Pair.create(ojc.mBluetoothAddress, "Time"), connect());
                    while(!mStreamSockets.get((new Pair(ojc.mBluetoothAddress, "Time"))).isOpen()) {
                        //wait for socket to connect asynchronously, (or skip sending data until connected)
                        ;
                    }
                    //*************************************************************************************
                    //New Socket Created
                    //Input First Message Information Here
                    // ....
                    //*************************************************************************************
                    mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, "Time"))).sendText("Time");
                    mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, "Time"))).sendText(String.valueOf(timeStamp));
                    mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, "Time"))).sendText("milisec");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (WebSocketException e) {
                    e.printStackTrace();
                }
            }*/

            //loop through all sensors in the object cluster, only considering the CAL formatted data
            sensorNames = ojc.mSensorNames;
            for (int i = 0; i < sensorNames.length; i++) {
                //if (i >= 0) break;
                if (sensorNames[i] != "") {
                    Collection<FormatCluster> formats = ojc.mPropertyCluster.get(sensorNames[i]);
                    //format = CAL, using calibrated data
                    FormatCluster formatCluster = ((FormatCluster) ojc.returnFormatCluster(formats, format));
                    //have a format cluster of CAL type for sensorNames[i]
                    if (formatCluster != null) {
                        double temp = formatCluster.mData;
                        //send data to the server
                        if (mStreamSockets.containsKey(Pair.create(ojc.mBluetoothAddress, sensorNames[i]))){
                            Log.d("Timing Experiment", "If entered");
                            if (mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, sensorNames[i]))).isOpen()) {
                                Log.d("Timing Experiment", "Send Message");
                                mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, sensorNames[i]))).sendText(String.valueOf(temp)); //server receives a triple of data (type, data, units) <- Not
                                Log.d("size_p", String.valueOf(String.valueOf(temp).length())+ " vs " + (String.valueOf(temp).getBytes(Charset.forName("UTF-8"))).length);
                               // mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, sensorNames[i]))).sendText(String.valueOf(temp));
                               // mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, sensorNames[i]))).sendText(formatCluster.mUnits);
                            }
                            else {
                                //socket exists but is not open
                                connectedNotOpenList.add(sensorNames[i]);
                            }
                        } else { //create socket for signal
                            try {
                                mStreamSockets.put(Pair.create(ojc.mBluetoothAddress, sensorNames[i]), connect());
                                while(!mStreamSockets.get((new Pair(ojc.mBluetoothAddress, sensorNames[i]))).isOpen()) {
                                    //wait for socket to connect asynchronously, (or skip sending data until connected)
                                    ;
                                }
                                Log.d("Timing Experiment", "Socket Created");
                                Log.d("wsconnection", "pass" + ojc.mBluetoothAddress + sensorNames[i]);
                                //*************************************************************************************
                                //New Socket Created
                                //Input First Message Information Here
                                // ....
                                //*************************************************************************************
                                // Added 9/17/18
                                //Send Identifiers *identifier = {userID, sensorID, type, humanSubjectType, timeStamp, done}
                                //mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, sensorNames[i]))).sendText(sensorNames[i] + '~' + String.valueOf(temp) + '~' + formatCluster.mUnits); //server receives a triple of data (type, data, units)

                                // Register a listener to receive WebSocket events.
                               /* mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, sensorNames[i]))).addListener(new WebSocketAdapter() {
                                    @Override
                                    public void onTextMessage(WebSocket websocket, String message) throws Exception {
                                        m = message;
                                        Log.d("onTextMessage", "Message0: " + m);
                                    }
                                });*/
                                // mStreamSockets.put(Pair.create(ojc.mBluetoothAddress, sensorNames[i]), connect());

                                while(serverMsg.equals("init")){
                                    //wait to receive streamID from server
                                    Log.d("onTextMessage", "Waiting: " + serverMsg);
                                }//don't need to know actual streamID (keep at unassigned=-1)

                                if(mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, sensorNames[i]))).getState() ==  WebSocketState.OPEN) {       //TODO check
                                mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, sensorNames[i]))).sendText("asdf@yahoo.com" + '~' + ojc.mBluetoothAddress + '~' + sensorNames[i] + '~' + formatCluster.mUnits + '~' + human_subject_type); //server receives identifiers
                                    //FIXME check human subject type on server
                                    //TODO get user id from email when login
                                    Log.d("onTextMessage", "IDs sent");
                                    //mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, sensorNames[i]))).sendText(sensorNames[i]); //server receives a triple of data (type, data, units)
                                    //mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, sensorNames[i]))).sendText(String.valueOf(temp));
                                    //mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, sensorNames[i]))).sendText(formatCluster.mUnits);
                                    //wait for streamID to arrive TODO
                                    while(serverMsg.equals("you connected!")){
                                        Log.d("onTextMessage", "Waiting");
                                    } //getstreamID
                                    Log.d("onTextMessage", "MessageInit: " + serverMsg);
                                    //ojc.mStreamID=serverMsg;
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (WebSocketException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
             }
        } else {
            if(mCloseSockets==true && mWebSocketStreaming==false)
            {
                sensorNames = ojc.mSensorNames;
                for (int i = 0; i < sensorNames.length; i++)
                {
                    if (sensorNames[i] != "") {
                        //mStreamSockets.get((Pair.create(ojc.mBluetoothAddress, sensorNames[i]))).disconnect();
                        //FIXME causes crash -> Pair doesn't exist anymore?

                    }
                }
            }   //TODO check if close works on database

            //start logging data locally because it is not being streamed
            if(mStoredFiles.containsKey(ojc.mBluetoothAddress)){
                mStoredFiles.get(ojc.mBluetoothAddress).logData(ojc);
            }else {
                //create logger
                mStoredFiles.put(ojc.mBluetoothAddress, new Logging(ojc.mBluetoothAddress, ojc.mBluetoothAddress + fromMilisecToDate(System.currentTimeMillis()), ",", "ShimmerOfflineLog"));
                mStoredFiles.get(ojc.mBluetoothAddress).logData(ojc);
            }

        }

        if(!connectedNotOpenList.isEmpty()) {
            //sockets are created but not connected
            //log data
            if(mStoredFiles.containsKey(ojc.mBluetoothAddress)){
                mStoredFiles.get(ojc.mBluetoothAddress).logData(ojc);
            }else {
                //create logger
                mStoredFiles.put(ojc.mBluetoothAddress, new Logging(ojc.mBluetoothAddress, ojc.mBluetoothAddress + fromMilisecToDate(System.currentTimeMillis()), ",", "TestShimmer"));
                mStoredFiles.get(ojc.mBluetoothAddress).logData(ojc);
            }
        }

        return ojc;
    }


    //**********************************************************************************************
    //* Functions that interact with Service
    //**********************************************************************************************

    /**
     * Connect WebSockets for Streaming of sensor data
     * accessed by a button in the SesnorProfile fragment of the UI
     */
    public void startWebSocketStreaming() {
        mWebSocketStreaming = true;
    }

    public void stopWebSocketStreaming() {
        mWebSocketStreaming = false;
        mCloseSockets = true;
    }

    public void streamStoredData(String bt, String signal) {
        streamStoredThread st = new streamStoredThread(bt, signal);
        st.run();
    }

    //Called in a thread
    public void startStreamingStoredData(String bt, String signal) {
        if(!mStoredFiles.containsKey(bt) || mStoredFiles.get(bt) == null)
            return;

        Logging logger =  mStoredFiles.get(bt);
        HashMap<String, WebSocket> StreamSockets = new HashMap<>();
        try {

            File outputFile = logger.getOutputFile();
            BufferedReader reader = new BufferedReader(new FileReader(outputFile));

            String line = null;
            String[] senNames = logger.mSensorNames;
            String[] senUnits = logger.mSensorUnits;

            //create sockets
            for(int i = 0; i < senNames.length; i++) {
                StreamSockets.put(senNames[i], connect());
            }
            for(int i = 0; i <senNames.length; i++) {
                //wait for sockets to connect
                while(!StreamSockets.get(senNames[i]).isOpen()) {
                    ;
                }
                //*************************************************************************************
                //New Socket Created
                //Input First Message Information Here
                // ....
                //*************************************************************************************
            }

            line = reader.readLine();
            Log.d("stream stored data:", "Line:" + line);
            if (line != null) {
                //get to the data
                while ((line != null) && (!line.startsWith("START_DATA_LOG"))) {
                    line = reader.readLine();
                }

                //prepare server for data streaming from this file
                while ((line = reader.readLine()) != null) {
                    //take apart the string
                    double[] data = fromLineToData(line, logger);

                    //stream the data to the server
                    for (int i = 0; i < data.length; i++) {
                        if (senNames.length <= i || senUnits.length <= i) continue;
                        if (StreamSockets.get(senNames[i]).isOpen()) {
                            StreamSockets.get(senNames[i]).sendText(String.valueOf(data[i]));
                        }
                        else {
                            //socket was open but is now closed
                            // Store data and try again later?
                        }
                    }

                }
            }
            //send closing sequence to the server
            //disconnect sockets
            for(int i = 0; i < senNames.length; i++) {
                StreamSockets.get(senNames[i]).disconnect();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (WebSocketException e) {
            e.printStackTrace();
        }

    }

    //**********************************************************************************************
    //* Helper Functions
    //**********************************************************************************************

    /**
     * Connect to the server.
     * returns a new webSocket
     */
    private WebSocket connect() throws IOException, WebSocketException {


        return new WebSocketFactory()
                .setConnectionTimeout(TIMEOUT)
                .createSocket(SERVER)
                /*.addListener(new WebSocketAdapter() {
                    // A text message arrived from the server.
                    public void onTextMessage(WebSocket websocket, String message) {
                        //serverMsg = message;
                        // TextView Counter = (TextView) findViewById(R.id.connectionCount);
                        // Counter.setText(message);
                    }
                })*/
                .addListener(new WebSocketAdapter() {
                    public void onTextMessage(WebSocket websocket, String message) {
                        serverMsg = message;
                        Log.d("onTextMessage", "MessageListener: " + serverMsg);
                        //popup here to show message FIXME

                    }
                })

                .addExtension(WebSocketExtension.PERMESSAGE_DEFLATE)
                .connectAsynchronously();
    }


    //convert the system time in miliseconds to a "readable" date format with the next format: YYYY MM DD HH MM SS
    public static String fromMilisecToDate(long miliseconds) {

        String date = "";
        Date dateToParse = new Date(miliseconds);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        date = dateFormat.format(dateToParse);

        return date;
    }

    private double[] fromLineToData(String line, Logging logger) {

        int lineLength = logger.mSensorNames.length;

        String[] dataFrag;
        dataFrag = line.split(logger.mDelimiter, lineLength);
        double[] data = new double[dataFrag.length];

        for (int i = 0; i < dataFrag.length; i++)
            data[i] = parseDouble(dataFrag[i]);

        return data;
    }

    //check internet connection
    public boolean isInternetAvailable() {

        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }
    }
   //takes a file with sensor data and streams that data to the server
    //deletes file when all the data has been streamed
    class streamStoredThread implements Runnable {
        String bt, signal;

        public streamStoredThread(String bt, String signal) {
            this.bt = bt;
            this.signal = signal;
        }

        /*
         * Defines the code to run for this task.
         */
        @Override
        public void run() {
            // Moves the current Thread into the background
            //android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
            startStreamingStoredData(bt, signal);
            if(mStoredFiles.containsKey(bt)) {
                deleteGenFile(mStoredFiles.get(bt));
                mStoredFiles.remove(bt);
            }
        }
    }

    //deletes a data file
    public boolean deleteGenFile(Logging logger) {

        logger.closeFile();
        return (logger.getOutputFile().delete());
    }

    //returns free storage size on the device in MB
    public long getFreeStorageSize() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable;
        if (android.os.Build.VERSION.SDK_INT >=
                android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
        } else {
            bytesAvailable = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
        }
        long megAvailable = bytesAvailable / (1024 * 1024);

        return megAvailable;
    }

    public long getStorageUsed() {
        if (!mStoredFiles.isEmpty()) {
            long sSize = 0;
            for (Object key: mStoredFiles.keySet()) {
                sSize += mStoredFiles.get(key).getOutputFile().getTotalSpace();
            }
            return (sSize / (1024 * 1024));
        }
        return -1;
    }

    public void rt_request_server(String bluetooth, String signal_name){
        Log.d("timer_event", "Start@: " + Long.toString(System.currentTimeMillis()));
        mStreamSockets.get((Pair.create(bluetooth, signal_name))).sendText("Do analysis");
        Log.d("onTextMessage", "RequestSent");
    }
    public void set_subject_type(String bluetooth, String subject_type){
        human_subject_type=subject_type;
    }

    public void send_done(String bluetooth, String signal_name){
        if(mStreamSockets.get((Pair.create(bluetooth, signal_name)))!=null) {
            mStreamSockets.get((Pair.create(bluetooth, signal_name))).sendText("Done");
            mStreamSockets.get((Pair.create(bluetooth, signal_name))).disconnect();// Added 3/18/19
            Log.d("onTextMessage", "Done sent " + signal_name);

        }

    }//1  9'
    public String retrieve_RT(){
        if(serverMsg.contains("NaiveBayes")){         //TODO make this generic
            Log.d("timer_event", "Back@ : " + Long.toString(System.currentTimeMillis()));
            return serverMsg;                      //TODO check if works 3/18/19 <=== CALL IN ASYNC TASK OF SENSOR PROFILE
        }
        else return null;
    }

    //    *'
}