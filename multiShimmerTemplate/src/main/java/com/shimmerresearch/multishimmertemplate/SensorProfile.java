package com.shimmerresearch.multishimmertemplate;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.shimmerresearch.android.Shimmer;
import com.shimmerresearch.database.DatabaseHandler;
import com.shimmerresearch.database.ShimmerConfiguration;
import com.shimmerresearch.driver.FormatCluster;
import com.shimmerresearch.driver.ObjectCluster;
import com.shimmerresearch.driver.ShimmerVerDetails;
import com.shimmerresearch.service.MultiShimmerTemplateService;

import java.util.Timer;
import java.util.concurrent.locks.Lock;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

//event analysis naming
import static com.shimmerresearch.android.Shimmer.TOAST;
import static com.shimmerresearch.android.ShimmerAdvance.fromMilisecToDate;
import static com.shimmerresearch.tools.EventAnalysis.*;
import static com.shimmerresearch.tools.UserAnalysis.*;
//event analysis classes
import com.shimmerresearch.tools.AnalysisManager;
import com.shimmerresearch.tools.EventAnalysis;
import com.shimmerresearch.tools.UserAnalysis;
import com.shimmerresearch.tools.sensor_data_buffer;
import com.shimmerresearch.tools.sensor_data_manager;
import com.shimmerresearch.multishimmertemplate.LoginActivity; //TODO CHECK

import org.apache.commons.lang3.ObjectUtils;
import org.w3c.dom.Text;

import javax.net.ssl.HttpsURLConnection;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by mingli-student on 1/30/2018.
 *
 */


public class SensorProfile extends Fragment{

    public View rootView = null;
    public MultiShimmerTemplateService mService;
    DatabaseHandler db;

    String deviceBluetoothAddress;
    int currentPosition;
    ShimmerConfiguration shimmerConfig;
    String mBluetoothAddress;
    //String streamID;

    TextView deviceName;
    TextView samplingRateText;
    TextView accelRange;
    TextView battVal;
    //Button serverStreamButton;
    ToggleButton serverStreamToggle;
    Button buttonSensorStreamDone;
    Button sensorProfileBack;
    Button streamStoredData;
    Button enableStatistics;
    Button enableEvent;
    Dialog serverStreamDialog;
    ListView streamDevicesList;
    TextView streamDeviceName;
    ListView streamSensorsList;
    View lateral_bar3;

    //initialized with values on shimmer configuration call in setup()
    static String[] deviceNames;
    static String[] deviceBluetoothAddresses;
    static String[][] mEnabledSensorNames;
    boolean firstTime = true;
    boolean streaming_to_server = false; //added 3/6/19
    static int lastSelectedDevice = 0;
    public int activeMotes;
    public int frequencyCounter = 0;
    public static final int FREQUENCY_COUNT = 5;

    private HashMap<String, List<analysisInfo>>[] mUserAnalysisMap;
    private HashMap<String, List<analysisInfo>>[] mCloudAnalysisMap;
    private HashMap<String, List<analysisInfo>>[] mEventAnalysisMap;
    private HashMap<String, List<Double>>[] mUserAnalResults;
    private HashMap<String, List<Double>>[] mCloudAnalResults;
    private HashMap<String, EventAnalysis>[] mEventAnalysis;
    private HashMap<String, UserAnalysis>[] mUserAnalysis;
    private HashMap<String, List<String>> mEventAnalysisNames;
    private HashMap<String, List<String>> mUserAnalysisNames;
    boolean[] mUserAnalysisActive;
    boolean[] mCloudAnalysisActive;
    boolean[] mEventAnalysisActive;
    boolean[] mStatsEnabled;
    private HashMap<String, Boolean>[] mStatSignal;


    private HashMap<String, Integer> mBluetoothToDevice;
    public static int numStats=7;
    public static HashMap<String, double[]>[] statArr;

    public static HashMap<String, sensor_data_manager>[] mData_managers;
    private HashMap<String, sensor_data_buffer>[] mBuffMap;
    public HashMap<String, Integer>[] mStatistics_enabled;
    public static double[] mStatistical_features = new double[7]; //7 running statistics
    public int mSegment_buffer_size = 10; //Default
    public AnalysisManager[] mAnalysis_managers;
    public AnalysisManager mAnalysis_manager;

    Timer batteryTimer;
    BattLogging mBattLog;
    Button batteryLogButton;


    //Analysis and Event naming
    //public static final String FALLINGEVENT = "FALLINGEVENT";


    //statistics names
    public static final String MEAN = "MEAN";
    public static final String VARIANCE = "VARIANCE";
    public static final String STANDARD_DEVIATION = "STANDARD_DEVIATION";
    public static final String MEDIAN = "MEDIAN";
    public static final String IQR = "IQR";
    public static final String MAX = "MAX";
    public static final String MIN = "MIN";

    Dialog statistics;
    Button buttonStatDone;
    Button selectMote;
    Dialog select_mote_list;
    Button selectMoteDone;
    ListView listOfMotes;
    ListView listStatSignals;
    ListView listStatDevices;
    static Dialog dialog_stats;
    Button dialogStatBack;
    static int selectedStatDevice=0;
    static int selectedStatSignal=0;
    Button userAnalysisButton;

    //For Real Time Analysis
    Button start_RT_Analysis;               //TODO use this button to send analysis request
    Dialog rt_analysis;
    Dialog rt_analysis_confirm;
    Button buttonRTDone;
    ListView listRTdevices;
    ListView listRTSignals;
    static int selectedRTdevice=0;
    static int selectedRTsignal=0;
    public TextView rt_confirm_device, rt_confirm_signal;
    Button rt_confirm_back;
    Button rt_confirm_confirm;
    String Rt_Request;

    Dialog event_detection;
    Button eventDetectionDone;
    ListView eventDetectionSignals;
    ListView eventDetectionAlg;
    static int lastSelectedEventSignal=0;

    Dialog set_prior_freq;
    ListView listPrior;
    ListView listFreq;
    Button buttonPriorFreqDone;
    public int priority=0;
    public int frequency=0;


    Dialog human_subject_type_dialog;
    Button hst_healthy;
    Button hst_patient;
    Button hst_confirm;

    Dialog rt_analysis_popup;
    TextView rt_results;
    Button rt_results_back;


    //Dialog user_analysis;
    Button userAnalysisDone;
    ListView userAnalysisSignals;
    ListView userAnalysisAlg;
    static int lastSelectedUserAnalysisSignal=0;

    String human_subject_type="";

    public String[] algoVal;
    static int lastSelectedSensorStat=0;
    public TextView StatMean, StatVariance, StatStdDev, StatMedian, StatIQR, StatMin, StatMax;

    //FIXME
    String userID="asdf@yahoo.com";

    //mutex
    Lock user_analysis;

    public SensorProfile() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fillAnalysisNames();


//		getActivity().invalidateOptionsMenu();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.sensor_profile, container, false);
        this.mService = ((MainActivity)getActivity()).mService;
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if(mService!=null){
            setup();
        }


        final AlertDialog.Builder dialogNoStreaming = new AlertDialog.Builder(getActivity()).setTitle("Error").setMessage("No device streaming")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        // TODO Auto-generated method stub
                        arg0.dismiss();
                    }
                });

        //deviceName = (TextView) rootView.findViewById(R.id.deviceName);
        samplingRateText = (TextView) rootView.findViewById(R.id.samplingRate);
        accelRange = (TextView) rootView.findViewById(R.id.accelRange);
        battVal = (TextView) rootView.findViewById(R.id.battVal);
        sensorProfileBack = (Button) rootView.findViewById(R.id.sensorProfileBack);

        // Set an EditText view to get user input
        final EditText editTextBattLimit = new EditText(getActivity());
        final EditText editTextDeviceName = new EditText(getActivity());
        editTextBattLimit.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        //mBluetoothAddress = shimmerConfig.getBluetoothAddress();
        /*String rate = Double.toString(mSamplingRateV);
        samplingRateText.setText("Sampling Rate "+"\n ("+rate+" Hz)");
        deviceName.setText("Device Name"+"\n ("+shimmerConfig.getDeviceName()+")");
        accelRange.setText("Acceleromete Range" + "\n ("+mAccelerometerRangeV+" g)");
        battVal.setText("Battery: " + "\n (" +Shimmer.SENSOR_BATT+") ");
*/
        sensorProfileBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
                Fragment fragment = new DevicesFragment();
                FragmentManager fragmentManager = getFragmentManager();
                String tag = "Devices";
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment, tag).commit();
                getActivity().getActionBar().setTitle(tag); //set the title of the window
            }
        });


        serverStreamDialog = new Dialog(getActivity());
        serverStreamDialog.setTitle("Stream to Server");
        serverStreamDialog.setContentView(R.layout.server_stream_list);

        //streamDevicesList = (ListView) serverStreamDialog.findViewById(R.id.streamDevicesList);
        //streamDevicesList.setChoiceMode(1);
        //streamDevicesList.setItemChecked(0, true);
        streamDeviceName = (TextView) serverStreamDialog.findViewById(R.id.streamDeviceName);
        //streamDeviceName.setText("      Sensor: " + shimmerConfig.getDeviceName() + "       ");


        //streamSensorsList = (ListView) serverStreamDialog.findViewById(R.id.streamSensorsList);
        //lateral_bar3 = (View) serverStreamDialog.findViewById(R.id.lateral_bar3);

        serverStreamToggle = (ToggleButton) rootView.findViewById(R.id.serverStreamToggle);
        serverStreamToggle.setChecked(false);

        human_subject_type_dialog = new Dialog(getActivity());
        human_subject_type_dialog.setContentView(R.layout.human_subject_type_dialog);
        hst_healthy = (Button) human_subject_type_dialog.findViewById(R.id.hst_healthy);
        hst_patient = (Button) human_subject_type_dialog.findViewById(R.id.hst_patient);

        hst_confirm = (Button) human_subject_type_dialog.findViewById(R.id.hst_confirm);





        serverStreamToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    human_subject_type = "";
                    hst_patient.setBackgroundColor(Color.LTGRAY);
                    hst_healthy.setBackgroundColor(Color.LTGRAY);
                    human_subject_type_dialog.show();
                    //TODO check

                } else {

                    //serverStreamToggle.setText("STREAM");
                    Log.d("toggle", "pass");

                    for (int i = 0; i < deviceBluetoothAddresses.length; i++) {
                        //mService.stopWebStreaming(deviceBluetoothAddresses[i]);
                        for(int j=0; j<mEnabledSensorNames[i].length; j++)
                        {
                            if(deviceBluetoothAddresses[i]!=null && mEnabledSensorNames[i][j]!=null){
                                mService.send_done(deviceBluetoothAddresses[i], mEnabledSensorNames[i][j]);
                                Log.d("onTextMessage", "done " + deviceBluetoothAddresses[i] + " " + mEnabledSensorNames[i][j]);
                            }
                        }
                        //TODO ++ stopWebStreaming? && stopStreaming?
                    }
                    streaming_to_server=false;
                    mService.stopStreamingAllDevices();
                    //ConnectToServer c = new ConnectToServer();
                    //c.execute("SendStop", userID);

                }
            }
        });


        hst_patient.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                hst_patient.setBackgroundColor(Color.CYAN);
                hst_healthy.setBackgroundColor(Color.LTGRAY);
                human_subject_type="patient";
            }
        });

        hst_healthy.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
               hst_healthy.setBackgroundColor(Color.CYAN);
               hst_patient.setBackgroundColor(Color.LTGRAY);
               human_subject_type = "healthy";
            }
        });

        hst_confirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("connectTryOk", human_subject_type);
                if(human_subject_type=="")
                {
                    Toast.makeText(getActivity(), "Please Select Subject Type", Toast.LENGTH_SHORT).show();
                }
                else {
                    for(int i=0; i<deviceBluetoothAddresses.length; i++)
                    {
                        //mService.startStreaming(deviceBluetoothAddresses[i]);
                        if(mService.noDevicesStreaming()) {
                            mService.startStreaming(deviceBluetoothAddresses[i]);   //TODO check if works
                        }
                        Log.d("connectTryOk", "streaming");
                        mService.set_subject_type(deviceBluetoothAddresses[i], human_subject_type); //TODO check if works
                        mService.startWebStreaming(deviceBluetoothAddresses[i]);
                        streaming_to_server=true;
                    }

                    human_subject_type_dialog.dismiss();
                    //human_subject_type="";
                }
            }
        });
        /*serverStreamButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                serverStreamDialog.show();
                //start streaming
                mService.startWebStreaming(deviceBluetoothAddresses[lastSelectedDevice]);

            }
             //FIXME check to see if working
        });*/


                //two new buttons
                streamStoredData = (Button) rootView.findViewById(R.id.Stream_Stored);

        streamStoredData.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.d("StreamStored Button", "Start Streaming called from service");
               //
                mService.streamStored(deviceBluetoothAddresses[lastSelectedDevice]);
                //}
                //FIXME check to see if working
            }

        });

        //enableStatistics = (Button) rootView.findViewById(R.id.Start_Stats);
        statistics = new Dialog(getActivity());
        statistics.setContentView(R.layout.statistics);
        dialog_stats = new Dialog(getActivity());
        dialog_stats.setContentView(R.layout.dialog_stats);
        dialogStatBack = (Button) dialog_stats.findViewById(R.id.dialogStatBack);
        listStatDevices = (ListView) statistics.findViewById(R.id.listStatDevices);
        listStatDevices.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        listStatSignals = (ListView) statistics.findViewById(R.id.listStatSignals);
        listStatSignals.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        listStatDevices.setItemChecked(0, true);
        buttonStatDone = (Button) statistics.findViewById(R.id.buttonStatDone);

        //selectMote = (Button) rootView.findViewById(R.id.selectMote);
        select_mote_list = new Dialog(getActivity());
        select_mote_list.setContentView(R.layout.select_mote_list);
        listOfMotes = (ListView) select_mote_list.findViewById(R.id.listOfMotes);
        listOfMotes.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        listOfMotes.setItemChecked(0, true);
        selectMoteDone = (Button) select_mote_list.findViewById(R.id.selectMoteDone);

        start_RT_Analysis = (Button) rootView.findViewById(R.id.start_RT_Analysis);
        rt_analysis = new Dialog(getActivity());
        rt_analysis.setContentView(R.layout.rt_analysis);
        buttonRTDone = (Button) rt_analysis.findViewById(R.id.buttonRTDone);
        listRTdevices= (ListView) rt_analysis.findViewById(R.id.listRTDevices);
        listRTSignals = (ListView) rt_analysis.findViewById(R.id.listRTSignals);
        listRTdevices.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        listRTSignals.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        listRTdevices.setItemChecked(0,true);

        //TODO check
        rt_analysis_confirm = new Dialog(getActivity());
        rt_analysis_confirm.setContentView(R.layout.rt_analysis_confirm);
        rt_confirm_device = (TextView) rt_analysis_confirm.findViewById(R.id.rt_confirm_device);
        rt_confirm_signal = (TextView) rt_analysis_confirm.findViewById(R.id.rt_confirm_signal);
        rt_confirm_back = (Button) rt_analysis_confirm.findViewById(R.id.rt_confirm_back);
        rt_confirm_confirm = (Button) rt_analysis_confirm.findViewById(R.id.rt_confirm_confirm);

        rt_analysis_popup = new Dialog(getActivity());
        rt_analysis_popup.setContentView(R.layout.rt_analysis_popup);
        rt_results = (TextView) rt_analysis_popup.findViewById(R.id.rt_results);
        rt_results_back = (Button) rt_analysis_popup.findViewById(R.id.rt_results_back);

        /*selectMote.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mService.noDevicesStreaming())
                    dialogNoStreaming.show();
                else {
                    ArrayAdapter<String> adapterMotes = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);
                    adapterMotes.addAll(deviceNames);
                    listOfMotes.setAdapter(adapterMotes);
                    listOfMotes.setItemChecked(lastSelectedDevice, true);
                    select_mote_list.setTitle("Select Device");
                    deviceName.setText(deviceNames[lastSelectedDevice]);
                    select_mote_list.show();
                }
            }
        });
        listOfMotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                lastSelectedDevice=i;
                deviceName.setText(deviceNames[i]);
                listOfMotes.setItemChecked(i, true);
            }
        });

        selectMoteDone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                select_mote_list.dismiss();
            }
        });*/

        start_RT_Analysis.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mService.noDevicesStreaming() || streaming_to_server == false)
                    dialogNoStreaming.show();
                else {
                    ArrayAdapter<String> adapterRTdevices = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);
                    adapterRTdevices.addAll(deviceNames);
                    listRTdevices.setAdapter(adapterRTdevices);

                   // ArrayAdapter<String> adapterRTsignals = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);
                    //adapterRTsignals.addAll(mEnabledSensorNames[selectedRTdevice]);
                    //listRTSignals.setAdapter(adapterRTsignals);
                    rt_analysis.show();
                }

            }

        });
        listRTdevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedRTdevice=i;
                listRTdevices.setItemChecked(i,true);
                ArrayAdapter<String> adapterRTsignals = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);
                adapterRTsignals.addAll(mEnabledSensorNames[selectedRTdevice]);
                listRTSignals.setAdapter(adapterRTsignals);
            }
        });

        listRTSignals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedRTsignal=i;
                listRTSignals.setItemChecked(i,true);
               //Rt_Request = userID + "~" + deviceBluetoothAddresses[selectedRTdevice] + "~" + mEnabledSensorNames[selectedRTdevice][selectedRTsignal];
                Rt_Request = deviceBluetoothAddresses[selectedRTdevice] + "~" + mEnabledSensorNames[selectedRTdevice][selectedRTsignal];
                //TODO make get userID from account
                rt_confirm_device.setText(deviceBluetoothAddresses[selectedRTdevice]);
                rt_confirm_signal.setText(mEnabledSensorNames[selectedRTdevice][selectedRTsignal]);
                rt_analysis_confirm.setTitle("Real Time Analysis Request");
                rt_analysis_confirm.show();
            }
        });

        buttonRTDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rt_analysis.dismiss();
            }
        });

        rt_confirm_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rt_analysis_confirm.dismiss();
            }
        });

        rt_confirm_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //"https://bsncloud.csufresno.edu/websockz/chat/ChatApp/appOp.php
                //send Rt_Request using URL + email + password -> "https://bsncloud.csufresno.edu/hunter_test/loginapp.php" || SERVER = "wss://bsncloud.csufresno.edu/wss2";
                //ConnectToServer c = new ConnectToServer();
                //c.execute("AnalysisRequest", Rt_Request);   //FIXME connecting to server is through shimmeradvance NOT URL
                mService.rt_request_server(deviceBluetoothAddresses[selectedRTdevice], mEnabledSensorNames[selectedRTdevice][selectedRTsignal]);
                //Log.d("timer_event", "Start1: " + Long.toString(System.currentTimeMillis()));
                rt_analysis_confirm.dismiss();
                GetAnalysisResults c = new GetAnalysisResults(); //Added 3/18/19
                //Log.d("timer_event", "time start");
                c.execute(deviceBluetoothAddresses[selectedRTdevice], mEnabledSensorNames[selectedRTdevice][selectedRTsignal]); //Added 3/18/19
            }
        });

        rt_results_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                rt_analysis_popup.dismiss();
            }
        });
       /* enableStatistics.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //mBattLog.logBattData();
                if(mService.noDevicesStreaming())
                    dialogNoStreaming.show();
                else {
                    ArrayAdapter<String> adapterstatdevices = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);
                    adapterstatdevices.addAll(deviceNames);
                    listStatDevices.setAdapter(adapterstatdevices);
                    statistics.show();
                }

            }

        });*/ //Removed 6/14/19 alongisde button, functionality moved to plot

        listStatDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedStatDevice=i;
                listStatDevices.setItemChecked(i, true);
                ArrayAdapter<String> adapterStatSignals = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);
                adapterStatSignals.addAll(mEnabledSensorNames[selectedStatDevice]);
                listStatSignals.setAdapter(adapterStatSignals);
            }
        });


        listStatSignals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedStatSignal=i;
                listStatSignals.setItemChecked(i,true);
                dialog_stats.setTitle(deviceNames[selectedStatDevice] + " " + mEnabledSensorNames[selectedStatDevice][i]);
                mStatsEnabled[selectedStatDevice] = true;
                HashMap<String, String[]> tempMap = new HashMap<String, String[]>();
                String[] tempString = {"MEAN", "VARIANCE", "STD_DEV", "MEDIAN", "IQR", "MAX", "MIN"};
                tempMap.put(mEnabledSensorNames[selectedStatDevice][i],tempString);

               /* lastSelectedSensorStat=i;
                dialog_stats.setTitle(deviceNames[lastSelectedDevice] + " " + mEnabledSensorNames[lastSelectedDevice][i]);

                mStatsEnabled[lastSelectedDevice] = true;
                HashMap<String, String[]> tempMap = new HashMap<String, String[]>();
                String[] tempString = {"MEAN", "VARIANCE", "STD_DEV", "MEDIAN", "IQR", "MAX", "MIN"};
                tempMap.put(mEnabledSensorNames[lastSelectedDevice][i],tempString);
                try {
                    setStatistics(tempMap, lastSelectedDevice);
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
                getStatsl(-1,mEnabledSensorNames[selectedStatDevice][i]); //set values to 0

                mStatistics_enabled[selectedStatDevice].put(mEnabledSensorNames[selectedStatDevice][selectedStatSignal], 1);

                dialog_stats.show();
            }
        });

        dialogStatBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_stats.dismiss();
                //disable statistics
                /*
                List<String> temp = new ArrayList<String>();
                temp.add(mEnabledSensorNames[lastSelectedDevice][lastSelectedSensorStat]);
                stopStatistics(temp, lastSelectedDevice);
                */
                mStatistics_enabled[selectedStatDevice].put(mEnabledSensorNames[selectedStatDevice][selectedStatSignal], 0);
            }
        });
        buttonStatDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statistics.dismiss();
            }
        });

        set_prior_freq = new Dialog(getActivity());
        set_prior_freq.setContentView(R.layout.set_prior_freq);
        listPrior = (ListView) set_prior_freq.findViewById(R.id.listPrior);
        listPrior.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        listFreq = (ListView) set_prior_freq.findViewById(R.id.ListFreq);
        listFreq.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        buttonPriorFreqDone = (Button) set_prior_freq.findViewById(R.id.buttonPriorFreqDone);


        event_detection = new Dialog(getActivity());
        event_detection.setContentView(R.layout.event_detection);
        eventDetectionSignals = (ListView) event_detection.findViewById(R.id.eventDetectionSignals);
        eventDetectionSignals.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        eventDetectionAlg = (ListView) event_detection.findViewById(R.id.eventDetectionAlg);
        eventDetectionAlg.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        enableEvent = (Button) rootView.findViewById(R.id.Enable_Event);
        eventDetectionDone = (Button) event_detection.findViewById(R.id.eventDetectionDone);

        enableEvent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mService.noDevicesStreaming())
                    dialogNoStreaming.show();
                else {
                    ArrayAdapter<String> adapterEventSignals = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);
                    adapterEventSignals.addAll(mEnabledSensorNames[lastSelectedDevice]);
                    eventDetectionSignals.setAdapter(adapterEventSignals);
                    event_detection.setTitle("Event Detection");
                    ArrayAdapter<String> adapterEventAlg = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);
                    eventDetectionAlg.setAdapter(adapterEventAlg);
                    event_detection.show();
                }
            }
        });
        eventDetectionSignals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                lastSelectedEventSignal=i;
                eventDetectionSignals.setItemChecked(i, true);
                ArrayAdapter<String> adapterEventAlg = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);
                if (mEventAnalysisNames.containsKey(mEnabledSensorNames[lastSelectedDevice][lastSelectedEventSignal])) {
                    adapterEventAlg.addAll(mEventAnalysisNames.get(mEnabledSensorNames[lastSelectedDevice][lastSelectedEventSignal]));//mEnabledSensorNames[lastSelectedDevice][i]));
                    eventDetectionAlg.setAdapter(adapterEventAlg);
                    if(!mEventAnalysisMap[lastSelectedDevice].containsKey(mEnabledSensorNames[lastSelectedDevice][lastSelectedEventSignal])){
                        List<analysisInfo> selectedAlgs= new ArrayList<analysisInfo>();
                        mEventAnalysisMap[lastSelectedDevice].put(mEnabledSensorNames[lastSelectedDevice][lastSelectedEventSignal],selectedAlgs);
                    }
                    else{
                        for(int itr=0; itr<mEventAnalysisNames.get(mEnabledSensorNames[lastSelectedDevice][lastSelectedEventSignal]).size(); itr++) {
                            for (analysisInfo anal : mEventAnalysisMap[lastSelectedDevice].get(mEnabledSensorNames[lastSelectedDevice][lastSelectedEventSignal])) {
                                if(mEventAnalysisNames.get((mEnabledSensorNames[lastSelectedDevice][lastSelectedEventSignal])).get(itr).equals(anal.mName)){
                                    eventDetectionAlg.setItemChecked(itr,true);
                                }
                            }

                        }
                    }
                }
                else{
                    eventDetectionAlg.setAdapter(adapterEventAlg);
                }

            }
        });



        buttonPriorFreqDone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                set_prior_freq.dismiss();
            }
        });

        eventDetectionDone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setEventAnalysis(lastSelectedDevice);
                event_detection.dismiss();
            }
        });

        eventDetectionAlg.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                boolean found=false;
                for (analysisInfo anal : mEventAnalysisMap[lastSelectedDevice].get(mEnabledSensorNames[lastSelectedDevice][lastSelectedEventSignal])){
                    if(anal.mName.equals((mEventAnalysisNames.get(mEnabledSensorNames[lastSelectedDevice][lastSelectedEventSignal]).get(i)))){
                        eventDetectionAlg.setItemChecked(i,false);
                        found = true;
                        mEventAnalysisMap[lastSelectedDevice].get(mEnabledSensorNames[lastSelectedDevice][lastSelectedEventSignal]).remove(anal);
                    }
                }
                if(!found)
                {
                    eventDetectionAlg.setItemChecked(i,true);

                    ArrayAdapter<String> adapterPrior = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);
                    ArrayAdapter<String> adapterFreq = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);

                    String[] priorVals = {"0", "1", "2", "3", "4", "5"};
                    String[] freqVals = {"0", "1", "2", "3", "4", "5"};
                    adapterPrior.addAll(priorVals);
                    adapterFreq.addAll(freqVals);

                    listPrior.setAdapter(adapterPrior);
                    listFreq.setAdapter(adapterFreq);
                    listPrior.setItemChecked(0, true);
                    listFreq.setItemChecked(0, true);
                    set_prior_freq.setTitle(mEventAnalysisNames.get(mEnabledSensorNames[lastSelectedDevice][lastSelectedEventSignal]).get(i));
                    set_prior_freq.show();

                    listPrior.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            priority=i;
                            listPrior.setItemChecked(i, true);

                        }
                    });

                    listFreq.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            frequency=i;
                            listFreq.setItemChecked(i, true);

                        }
                    });

                    analysisInfo newAnalInfo = new analysisInfo(priority, mEventAnalysisNames.get(mEnabledSensorNames[lastSelectedDevice][lastSelectedEventSignal]).get(i), frequency);
                    mEventAnalysisMap[lastSelectedDevice].get(mEnabledSensorNames[lastSelectedDevice][lastSelectedEventSignal]).add(newAnalInfo);
                }

            }
        });

        //user_analysis = new Dialog(getActivity());
        //user_analysis.setContentView(R.layout.user_analysis);
        //userAnalysisSignals = (ListView) user_analysis.findViewById(R.id.userAnalysisSignals);
        //userAnalysisSignals.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        //userAnalysisAlg = (ListView) user_analysis.findViewById(R.id.userAnalysisAlg);
        //userAnalysisAlg.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        //userAnalysisDone = (Button) user_analysis.findViewById(R.id.userAnalysisDone);
       // userAnalysisButton = (Button) rootView.findViewById(R.id.User_Analysis);
/*
        userAnalysisButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //Fragment fragment = new DataAnalysisFragment();
                //getFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();
                if(mService.noDevicesStreaming())
                    dialogNoStreaming.show();
                else {
                    ArrayAdapter<String> adapterUserAnalysisSignals = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);
                    adapterUserAnalysisSignals.addAll(mEnabledSensorNames[lastSelectedDevice]);
                    userAnalysisSignals.setAdapter(adapterUserAnalysisSignals);
                    user_analysis.setTitle("User Analysis");
                    ArrayAdapter<String> adapterUserAnalysisAlg = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);
                    userAnalysisAlg.setAdapter(adapterUserAnalysisAlg);
                    user_analysis.show();
                }

            }
        });

        userAnalysisSignals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                lastSelectedUserAnalysisSignal=i;
                userAnalysisSignals.setItemChecked(i, true);
                ArrayAdapter<String> adapterUserAnalysisAlg = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);
                if (mUserAnalysisNames.containsKey(mEnabledSensorNames[lastSelectedDevice][lastSelectedUserAnalysisSignal])) {
                    adapterUserAnalysisAlg.addAll(mUserAnalysisNames.get(mEnabledSensorNames[lastSelectedDevice][lastSelectedUserAnalysisSignal]));//mEnabledSensorNames[lastSelectedDevice][i]));
                    userAnalysisAlg.setAdapter(adapterUserAnalysisAlg);
                    if(!mUserAnalysisMap[lastSelectedDevice].containsKey(mEnabledSensorNames[lastSelectedDevice][lastSelectedUserAnalysisSignal])){
                        List<analysisInfo> selectedAlgs= new ArrayList<analysisInfo>();
                        mUserAnalysisMap[lastSelectedDevice].put(mEnabledSensorNames[lastSelectedDevice][lastSelectedUserAnalysisSignal],selectedAlgs);
                    }
                    else{
                        for(int itr=0; itr<mUserAnalysisNames.get(mEnabledSensorNames[lastSelectedDevice][lastSelectedUserAnalysisSignal]).size(); itr++) {
                            for (analysisInfo anal : mUserAnalysisMap[lastSelectedDevice].get(mEnabledSensorNames[lastSelectedDevice][lastSelectedUserAnalysisSignal])) {
                                if(mUserAnalysisNames.get((mEnabledSensorNames[lastSelectedDevice][lastSelectedUserAnalysisSignal])).get(itr).equals(anal.mName)){
                                    userAnalysisAlg.setItemChecked(itr,true);
                                }
                            }

                        }
                    }
                }
                else{
                    userAnalysisAlg.setAdapter(adapterUserAnalysisAlg);
                }

            }
        });

        userAnalysisAlg.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                boolean found=false;
                for (analysisInfo anal : mUserAnalysisMap[lastSelectedDevice].get(mEnabledSensorNames[lastSelectedDevice][lastSelectedUserAnalysisSignal])){
                    if(anal.mName.equals((mUserAnalysisNames.get(mEnabledSensorNames[lastSelectedDevice][lastSelectedUserAnalysisSignal]).get(i)))){
                        userAnalysisAlg.setItemChecked(i,false);
                        found = true;
                        mUserAnalysisMap[lastSelectedDevice].get(mEnabledSensorNames[lastSelectedDevice][lastSelectedUserAnalysisSignal]).remove(anal);
                    }
                }
                if(!found)
                {
                    userAnalysisAlg.setItemChecked(i,true);

                    ArrayAdapter<String> adapterPrior = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);
                    ArrayAdapter<String> adapterFreq = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1);

                    String[] priorVals = {"0", "1", "2", "3", "4", "5"};
                    String[] freqVals = {"0", "1", "2", "3", "4", "5"};
                    adapterPrior.addAll(priorVals);
                    adapterFreq.addAll(freqVals);

                    listPrior.setAdapter(adapterPrior);
                    listFreq.setAdapter(adapterFreq);
                    listPrior.setItemChecked(0, true);
                    listFreq.setItemChecked(0, true);
                    set_prior_freq.setTitle(mUserAnalysisNames.get(mEnabledSensorNames[lastSelectedDevice][lastSelectedUserAnalysisSignal]).get(i));
                    set_prior_freq.show();

                    listPrior.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            priority=i;
                            listPrior.setItemChecked(i, true);

                        }
                    });

                    listFreq.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            frequency=i;
                            listFreq.setItemChecked(i, true);

                        }
                    });

                    analysisInfo newAnalInfo = new analysisInfo(priority, mUserAnalysisNames.get(mEnabledSensorNames[lastSelectedDevice][lastSelectedUserAnalysisSignal]).get(i), frequency);
                    mUserAnalysisMap[lastSelectedDevice].get(mEnabledSensorNames[lastSelectedDevice][lastSelectedUserAnalysisSignal]).add(newAnalInfo);
                }

            }
        });

        userAnalysisDone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setUserAnalysis(lastSelectedDevice);
                user_analysis.dismiss();
            }
        });


        /*enableEvent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //mService.enableEvent(deviceBluetoothAddress);
                mEventAnalysisActive[0] = true;
                List<String> tempL = new ArrayList<String>();
                tempL.add(FALLINGEVENT);
                mEventAnalysisMap[0].put("Low Noise Accelerometer X", tempL);
                createSensorBuffer("Low Noise Accelerometer X","CAL", "m/s^2", 100, 0);
            }

        });
*/

        buttonSensorStreamDone = (Button) serverStreamDialog.findViewById(R.id.buttonSensorStreamDone);
        buttonSensorStreamDone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                serverStreamDialog.dismiss();
            }
        });


       /* batteryLogButton = (Button) rootView.findViewById(R.id.batteryLogButton);
        batteryLogButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                mBattLog = new BattLogging(fromMilisecToDate(System.currentTimeMillis()) +"BatteryLog", ",", "BatteryData", "5 sig 15 analysis");
                //Toast.makeText(getActivity(), "Logging Battery", Toast.LENGTH_SHORT).show();
                timerRun.run();
            }
        });*/


        return rootView;

    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d("Activity Name",activity.getClass().getSimpleName());

        if (!isMyServiceRunning()){
            Intent intent=new Intent(getActivity(), MultiShimmerTemplateService.class);
            getActivity().startService(intent);
        }
    }


    protected boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("com.shimmerresearch.service.MultiShimmerTemplateService".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public void updateShimmerConfigurationList(List<ShimmerConfiguration> shimmerConfigurationList) {
        //save configuration settings
        db.saveShimmerConfigurations("Temp", mService.mShimmerConfigurationList);
        //query service get the deviceNames and Bluetooth addresses which are streaming
        shimmerConfigurationList = mService.getStreamingDevices();
        deviceNames = new String[shimmerConfigurationList.size()];
        deviceBluetoothAddresses = new String[shimmerConfigurationList.size()];
        mEnabledSensorNames = new String[shimmerConfigurationList.size()][Shimmer.MAX_NUMBER_OF_SIGNALS];// up to 9 (eg accel x, accel y , accel z, gyro...,mag...,ExpB0,ExpB7

        int pos=0;
        for (ShimmerConfiguration sc:shimmerConfigurationList){
            deviceNames[pos]=sc.getDeviceName();
            deviceBluetoothAddresses[pos]=sc.getBluetoothAddress();
            Shimmer shimmer = mService.getShimmer(deviceBluetoothAddresses[pos]);
            mEnabledSensorNames[pos]=shimmer.getListofEnabledSensorSignals();
            pos++;
        }

        activeMotes = shimmerConfigurationList.size();
        //initialize arrays
        mUserAnalysisMap = new HashMap[activeMotes];
        mCloudAnalysisMap = new HashMap[activeMotes];
        mUserAnalResults = new HashMap[activeMotes];
        mCloudAnalResults = new HashMap[activeMotes];
        mEventAnalysisMap = new HashMap[activeMotes];
        mBuffMap = new HashMap[activeMotes];
        mEventAnalysis = new HashMap[activeMotes];
        mUserAnalysis = new HashMap[activeMotes];
        mUserAnalysisActive = new boolean[activeMotes];
        mCloudAnalysisActive = new boolean[activeMotes];
        mEventAnalysisActive = new boolean[activeMotes];
        mStatsEnabled = new boolean[activeMotes];
        mBluetoothToDevice = new HashMap<String, Integer>();
        statArr = new HashMap[activeMotes];
        mStatSignal = new HashMap[activeMotes];

        mData_managers = new HashMap[activeMotes];
        mStatistics_enabled = new HashMap[activeMotes];
        //initialize maps
        for (int i = 0; i < activeMotes; i++) {
            mUserAnalysisMap[i] = new HashMap<String, List<analysisInfo>>();
            mCloudAnalysisMap[i] = new HashMap<String, List<analysisInfo>>();
            mUserAnalResults[i] = new HashMap<String, List<Double>>();
            mCloudAnalResults[i] = new HashMap<String, List<Double>>();
            mEventAnalysisMap[i] = new HashMap<String, List<analysisInfo>>();
            mBuffMap[i] = new HashMap<String, sensor_data_buffer>();
            mEventAnalysis[i] = new HashMap<String, EventAnalysis>();
            mUserAnalysis[i] = new HashMap<String, UserAnalysis>();
            mBluetoothToDevice.put(deviceBluetoothAddresses[i], i);
            statArr[i] = new HashMap<String, double[]>();
            mStatSignal[i] = new HashMap<>();

            mData_managers[i] = new HashMap<>();
            mStatistics_enabled[i] = new HashMap<>();
        }
    }

    public void onPause(){
        super.onPause();
        if (mService!=null){
            db.saveShimmerConfigurations("Temp", mService.mShimmerConfigurationList);
        }
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Shimmer.MESSAGE_READ:
                    if ((msg.obj instanceof ObjectCluster)) {    // within each msg an object can be include, objectclusters are used to represent the data structure of the shimmer device
                        ObjectCluster objectCluster = (ObjectCluster) msg.obj;

                        int index = mBluetoothToDevice.get(objectCluster.mBluetoothAddress);    //FIXME may crash when adding a new sensor after visiting sensor profile

                        //if(!(objectCluster.mStreamID.equals("init") || objectCluster.mStreamID.equals("you connected!") || objectCluster.mStreamID.equals(null)))
                        //{
                        //    streamID=objectCluster.mStreamID;
                        //}
                        //read sensor data into buffers, if needed for analysis or statistics
                        if (mUserAnalysisActive[index] || mEventAnalysisActive[index] || mStatsEnabled[index]) {
                            ByteBuffer wrapped = ByteBuffer.wrap(objectCluster.mSystemTimeStamp);
                            double timeStamp = wrapped.getDouble();

                            for (String key : mBuffMap[index].keySet()) {
                                sensor_data_buffer buff = mBuffMap[index].get(key);
                                Collection<FormatCluster> formats = objectCluster.mPropertyCluster.get(key);
                                FormatCluster formatCluster = ((FormatCluster) objectCluster.returnFormatCluster(formats, buff.mFormat));

                                if (formatCluster != null) {
                                    Log.d("stats_q", "format cluster ex");
                                    double temp = formatCluster.mData;
                                    if(buff.addData(key, temp, timeStamp)) {
                                        if(!mData_managers[index].containsKey(key)) {
                                            mData_managers[index].put(key, new sensor_data_manager(mSegment_buffer_size, key));
                                        }
                                        mData_managers[index].get(key).add_segment(buff.flushData(key));


                                        if(mStatistics_enabled[index].containsKey(key) && mStatistics_enabled[index].get(key) > 1) {
                                            mStatistical_features = mData_managers[index].get(key).get_last_segment_features().mStatistic_features;
                                            getStatsl(index, key); //update statistics
                                        }
                                    }
                                    if(mStatistics_enabled[index].containsKey(key) && mStatistics_enabled[index].get(key) == 1) {
                                        Log.d("stats_q", "stats enabled");
                                        mStatistics_enabled[index].put(key,2);
                                        //update statistics
                                        if(mData_managers[index].containsKey(key)) {
                                            mStatistical_features = mData_managers[index].get(key).get_last_segment_features().mStatistic_features;
                                            getStatsl(index, key);
                                        }
                                    }
                                }

                            }
                        }
                    }
                    break;
            }
        }
    };

    public void setup(){
        db=mService.mDataBase;
        mService.mShimmerConfigurationList = db.getShimmerConfigurations("Temp");


        if (firstTime){
            updateShimmerConfigurationList(mService.mShimmerConfigurationList);
            firstTime=false;
        }

        mService.setSenProfHandler(mHandler);

        /******************************************************************************************
         * Experiment setup
         * ON, ON2, O2N are the three analysis algorithms referring to their time complexity
         */
    /*
         List<analysisInfo> tempL = new ArrayList<analysisInfo>();
            //add analysis
            tempL.add(new analysisInfo(0,ON2,0));
            tempL.add(new analysisInfo(0,ON2,0));
            tempL.add(new analysisInfo(0,ON2,0));
            //tempL.add(new analysisInfo(0,O2N,0));
            //tempL.add(new analysisInfo(0,O2N,0));
            mEventAnalysisMap[lastSelectedDevice].put(mEnabledSensorNames[lastSelectedDevice][0], tempL);
        mEventAnalysisMap[lastSelectedDevice].put(mEnabledSensorNames[lastSelectedDevice][1], tempL);
        mEventAnalysisMap[lastSelectedDevice].put(mEnabledSensorNames[lastSelectedDevice][2], tempL);
        mEventAnalysisMap[lastSelectedDevice].put(mEnabledSensorNames[lastSelectedDevice][3], tempL);
        mEventAnalysisMap[lastSelectedDevice].put(mEnabledSensorNames[lastSelectedDevice][4], tempL);
        setEventAnalysis(lastSelectedDevice);

    */
        //mService.startWebStreaming(deviceBluetoothAddresses[lastSelectedDevice]);
        //TODO set this so streaming only start on button || before streaming starts, send identifiers first

    }

    Handler timerHandler = new Handler();
    Runnable timerRun = new Runnable() {
        @Override
        public void run() {
            mBattLog.logBattData();
            timerHandler.postDelayed(this, 5000);
        }
    };


    public void fillAnalysisNames(){
        String[] sigL = Shimmer.getListofSupportedSensors(ShimmerVerDetails.HW_ID.SHIMMER_3);
        List<String> analysisL = new ArrayList<String>();
        List<String> eventL = new ArrayList<String>();
        mUserAnalysisNames = new HashMap<String, List<String>>();
        mEventAnalysisNames = new HashMap<String, List<String>>();

        analysisL.add(DISTANCE);
        //eventL.add(FALLINGEVENT);
        mUserAnalysisNames.put("Low Noise Accelerometer X", analysisL);
        mEventAnalysisNames.put("Low Noise Accelerometer X", eventL);

        for (int i = 0; i < sigL.length; i++) {
            mEventAnalysisNames.put(sigL[i], eventL);
        }
        for (int i = 0; i < sigL.length; i++) {
            mUserAnalysisNames.put(sigL[i], analysisL);
        }
        //analysisL.clear();
        //eventL.clear();  FIXME clear clears stuff even after entered

        //next signal

    }

    public void setEventAnalysis(int index) {
        //Set<String> current = mEventAnalysis[index].keySet(); TODO fix logic for checking if analysis API already exists
        mEventAnalysisActive[index] = true;
        for (String sig : mEventAnalysisMap[index].keySet()) {
            //current.remove(sig);
            if (mEventAnalysis[index].containsKey(sig)) {
                for (analysisInfo anal: mEventAnalysisMap[index].get(sig)) {
                    mEventAnalysis[index].get(sig).AddEvent(anal);
                }
            }
            else {
                mEventAnalysis[index].put(sig, new EventAnalysis(mEventAnalysisMap[index].get(sig)));
                createSensorBuffer(sig, "CAL", "m/s", 100, index); //TODO fix units for sensor buffer
                Toast.makeText(getActivity(), "Setup", Toast.LENGTH_SHORT).show();
            }
        }
       // for (String eve: current) {
        //    mEventAnalysis[index].remove(eve);
       // }
    }

    public void setUserAnalysis(int index) {
        mUserAnalysisActive[index] = true;
        //Set<String> current = mUserAnalysis[index].keySet();
        for (String sig : mUserAnalysisMap[index].keySet()) {
            //current.remove(sig);
            if (mUserAnalysis[index].containsKey(sig)) {
                for (analysisInfo anal: mUserAnalysisMap[index].get(sig)) {
                    mUserAnalysis[index].get(sig).AddAnalysis(anal);
                }
            }
            else {
                mUserAnalysis[index].put(sig, new UserAnalysis(mUserAnalysisMap[index].get(sig)));
                createSensorBuffer(sig, "CAL", "m/s", 100, index); //TODO fix units for sensor buffer
            }
        }
        //for (String anal: current) {
        //    mUserAnalysis[index].remove(anal);
        //}
    }



    //called when analysis algorithms are first turned on for a given signal
    //if the buffer already exists, the function will exit
    //  TODO if the buffer exists but the size is not large enough for the given analysis algorithm
    //       increase the size of the buffer
    public void createSensorBuffer(String signal, String format, String units, int size, int index) {
        if (!mBuffMap[index].containsKey(signal)) {
            sensor_data_buffer buff = new sensor_data_buffer(signal, format, units, size);
            mBuffMap[index].put(signal, buff);
        }
        /*
        //if another analysis algorithm requires a larger buffer, dynamically increase the buffer size of the currently existing buffer
        else if(mBuffMap[index].get(signal).BUFFSIZE < size) {
            //increase buffer size
            mBuffMap[index].get(signal).BUFFSIZE = size;
        }*/
    }



    static public void getStatsl(int i, String sig){		//maybe call this when stat button pressed??
        //properties = new HashMap<String, Map<String, Map<String,String>>>();
        //properties.put("a", new HashMap<String, Map<String,String>>());
        //properites.get("a").put("b", new HashMap<String,String>());
        Log.d("stats_q","enter func");

        TextView StatMean, StatVariance, StatStdDev, StatMedian, StatIQR, StatMin, StatMax;

        StatMean = (TextView) dialog_stats.findViewById(R.id.StatMean);
        StatVariance = (TextView) dialog_stats.findViewById(R.id.StatVariance);
        StatStdDev = (TextView) dialog_stats.findViewById(R.id.StatStdDev);
        StatMedian = (TextView) dialog_stats.findViewById(R.id.StatMedian);
        StatIQR = (TextView) dialog_stats.findViewById(R.id.StatIQR);
        StatMin = (TextView) dialog_stats.findViewById(R.id.StatMin);
        StatMax = (TextView) dialog_stats.findViewById(R.id.StatMax);
        if (i != selectedStatDevice && i!=-1 ) return;
        Log.d("stats_q","enter func2");


        /*if (statArr[i].containsKey(sig)) {

            Log.d("stats_q","enter statArr");
            String temp;
            temp = "Mean: " + sig + ": " + statArr[i].get(sig)[0];
            StatMean.setText(temp);
            temp = "Var: " + sig + ": " + statArr[i].get(sig)[1];
            StatVariance.setText(temp);
            temp = "Std_Dev: " + sig + ": " + statArr[i].get(sig)[2];
            StatStdDev.setText(temp);
            temp = "Median: " + sig + ": " + statArr[i].get(sig)[3];
            StatMedian.setText(temp);
            temp = "IQR: " + sig + ": " + statArr[i].get(sig)[4];
            StatIQR.setText(temp);
            temp = "Max: " + sig + ": " + statArr[i].get(sig)[5];
            StatMax.setText(temp);
            temp = "Min: " + sig + ": " + statArr[i].get(sig)[6];
            StatMin.setText(temp);
        }*/


        if (i == -1) {
            String temp;
            Log.d("stats_q","enter init");
            temp = "Mean: " + sig + ": " + 0;
            StatMean.setText(temp);
            temp = "Var: " + sig + ": " + 0;
            StatVariance.setText(temp);
            temp = "Std_Dev: " + sig + ": " + 0;
            StatStdDev.setText(temp);
            temp = "Median: " + sig + ": " + 0;
            StatMedian.setText(temp);
            temp = "IQR: " + sig + ": " + 0;
            StatIQR.setText(temp);
            temp = "Max: " + sig + ": " + 0;
            StatMax.setText(temp);
            temp = "Min: " + sig + ": " + 0;
            StatMin.setText(temp);
        }
        else {
            String temp;
            temp = "Mean: " + sig + ": " + mStatistical_features[0];
            StatMean.setText(temp);
            temp = "Var: " + sig + ": " + mStatistical_features[1];
            StatVariance.setText(temp);
            temp = "Std_Dev: " + sig + ": " + mStatistical_features[2];
            StatStdDev.setText(temp);
            temp = "Median: " + sig + ": " + mStatistical_features[3];
            StatMedian.setText(temp);
            temp = "IQR: " + sig + ": " + mStatistical_features[4];
            StatIQR.setText(temp);
            temp = "Max: " + sig + ": " + mStatistical_features[5];
            StatMax.setText(temp);
            temp = "Min: " + sig + ": " + mStatistical_features[6];
            StatMin.setText(temp);
        }
    }

    public class analysisInfo {
        public int mPriotity;
        public String mName;
        public int mFrequency;

        public analysisInfo(int priority, String name, int frequency) {
            mPriotity = priority;
            mName = name;
            mFrequency = frequency;
        }
    }

    //always running when analysis is activated, schedules one analysis at a time, with sleep() for battery adaptations and wait() when User Requested analysis is called
    class analysisManagerThread implements Runnable {

        public analysisManagerThread() {
        }

        /*
         * Defines the code to run for this task.
         */
        @Override
        public void run() {
            // Moves the current Thread into the background
            //android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
            while(true) {
                user_analysis.lock(); try { //make sure user analysis is not being executed
                    if (mAnalysis_manager.Bt_ready) {
                        //schedule inactivity for battery preservation
                        wait(mAnalysis_manager.Bt_time);
                        mAnalysis_manager.Bt_ready = false;
                        mAnalysis_manager.Bt_relative_deadline = mAnalysis_manager.Bt_relative_deadline + mAnalysis_manager.Bt_period;
                    }
                    else {
                        //schedule an analysis
                        mAnalysis_manager.run_analysis();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    user_analysis.unlock();
                }
            }
        }
    }


    public class BattLogging{
        boolean mFirstWrite=true;
        public String[] mSensorNames;
        public String[] mSensorFormats;
        public String[] mSensorUnits;
        String mFileName="";
        String mTrialName="";
        BufferedWriter writer=null;
        public File outputFile;
        public String mDelimiter=","; //default is comma
        //records data entries
        public int mDataSize = 0;

        float currentBatteryLevel=101;



        public BattLogging(String myName,String delimiter, String folderName, String trialName){
            mFileName=myName;
            mDelimiter=delimiter;
            mTrialName = trialName;


            File root = new File(Environment.getExternalStorageDirectory() + "/"+folderName);

            if(!root.exists())
            {
                if(root.mkdir()); //directory is created;
            }
            //changed to text file as opposed to .dat
            // outputFile = new File(root, mFileName+".dat");
            outputFile = new File(root, mFileName+".txt");
        }

        public boolean getBattData(Context context)
        {
            IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);

            Intent batteryStatus = context.registerReceiver(null, ifilter);

            if(batteryStatus!=null)
            {
                int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1 );
               // int en = BatteryManager.BATTERY_PROPERTY_ENERGY_COUNTER;
               // int cr = BatteryManager.BATTERY_PROPERTY_CURRENT_AVERAGE;
               // int crn = BatteryManager.BATTERY_PROPERTY_CURRENT_NOW;
                float batteryPct = level/(float)scale;

                if(batteryPct<currentBatteryLevel){
                    currentBatteryLevel=batteryPct;
                    return true;
                }
                //return (batteryPct*100);
                //return(crn);
            }
            return false;
            //return 0;
        }

        public void logBattData()
        {
            //Toast.makeText(getActivity(), "Log Batt", Toast.LENGTH_SHORT).show();
            try{
                if(mFirstWrite==true)
                {
                    writer=new BufferedWriter(new FileWriter(outputFile));
                    writer.write(mTrialName);
                    writer.newLine();
                    if(getBattData(getActivity().getApplicationContext()))
                    {
                        writer.write(fromMilisecToDate(System.currentTimeMillis()));
                        writer.newLine();
                        writer.write(Float.toString(currentBatteryLevel));
                        writer.newLine();
                    }

                    mFirstWrite=false;
                }
                else {

                    if(getBattData(getActivity().getApplicationContext()))
                    {
                        writer.write(fromMilisecToDate(System.currentTimeMillis()));
                        writer.newLine();
                        writer.write(Float.toString(currentBatteryLevel));
                        writer.newLine();

                    }


                }
                //flush data after each write so we do not need to worry about closing the file
                writer.flush();

            }catch(IOException e){
                e.printStackTrace();;
                Log.d("Battery", "Error BufferedWriter");
            }

        }

    }
    class GetAnalysisResults extends AsyncTask<String,String,String>{         //Added 3/18/19
        String rt_analysis_results=null;
        long start_time = System.currentTimeMillis();

        @Override
        protected String doInBackground(String... params){
            Log.d("timer_event", "function start : " + Long.toString(start_time));
            String bt_choice = params[0];
            String sig_choice = params[1];
            while(rt_analysis_results==null)
            {
                rt_analysis_results=mService.retrieve_RT(bt_choice);
            }
            Log.d("onText", "Analysis_Received" + rt_analysis_results);
            //rt_results.setText(rt_analysis_results);          //FIXME do this outside asynctask <-- after it returns??
            //rt_analysis_popup.show();
            //Toast.makeText(getActivity(), "Analysis Received: " + rt_analysis_results, Toast.LENGTH_SHORT).show();

            long m_time = System.currentTimeMillis()-start_time;
            Log.d("timer_event", Long.toString(m_time));
            int seconds = (int) (m_time / 1000);
            Log.d("timer_event",seconds + "s");
            int minutes = seconds / 60;
            int m_seconds = seconds % 60;
            Log.d("timer_event",minutes + "m" + m_seconds + "s");

            return rt_analysis_results;
        }
        protected void onPostExecute(String rt_analysis_results){
            super.onPostExecute(rt_analysis_results);
            //Toast.makeText(getActivity(), "Analysis Received: " + rt_analysis_results, Toast.LENGTH_SHORT).show();
            /*long time = System.currentTimeMillis()-start_time;
            Log.d("timer_event", Long.toString(time));
            int seconds = (int) (time / 1000);
            Log.d("timer_event",seconds + "s");
            int minutes = seconds / 60;
            int m_seconds = seconds % 60;
            Log.d("timer_event",minutes + "m" + m_seconds + "s");*/
            rt_results.setText(rt_analysis_results);          //FIXME do this outside asynctask <-- after it returns??
            rt_analysis_popup.setTitle("Analysis Received");
            rt_analysis_popup.show();
        }
    }

    class ConnectToServer extends AsyncTask<String,String,Void> {

        @Override
        protected Void doInBackground(String... params) {
            String choice = params[0];      //AnalysisRequest
            String add_param = params[1];      //userID + "~" + deviceBluetoothAddresses[selectedRTdevice] + "~" + mEnabledSensorNames[selectedRTdevice][selectedRTsignal]
            URL url = null;
            //String password_input = params[1];
           // String db_return = "";
            //int tmp;

            try {
                if(choice=="SendStop") {
                    Log.d("connectTryOk", "We Tried Stop");
                    String urlParams = "?operation=stop&user=" + add_param; //add_param=userID
                    //FIXME get UserID from account
                    url = new URL("https://bsncloud.csufresno.edu/websockz/chat/ChatApp/appOp.php" + urlParams);
                    Log.d("connectTryOk", "https://bsncloud.csufresno.edu/websockz/chat/ChatApp/appOp.php" + urlParams);
                    HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                    urlConnection.connect();
                    int responseCode = urlConnection.getResponseCode();
                    Log.d("connectTryOk2", urlConnection.getURL().toString());
                }
                else if (choice=="AnalysisRequest")
                {
                    Log.d("connectTryOk", "We Tried Analysis");
                    //String urlParams = "?operation=Analysis&Req="+ add_param + "~" + streamID;   //add_param=Rt_Request FIXME can remove streamID?
                    //url = new URL("https://bsncloud.csufresno.edu/websockz/chat/ChatApp/appOp.php" + urlParams);
                    //Log.d("connectTryOk", "https://bsncloud.csufresno.edu/websockz/chat/ChatApp/appOp.php" + urlParams);
                    //HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                    //urlConnection.connect();
                    //int responseCode = urlConnection.getResponseCode();
                    //Log.d("connectTryOk2", urlConnection.getURL().toString());
                    //downloading file
                    //InputStream input = urlConnection.getInputStream();
                    //Toast.makeText(getActivity(), "Analysis Received: " + input, Toast.LENGTH_SHORT).show();
                    //rt_results.setText(input.toString());
                    //rt_analysis_popup.show();


                    /*
                    String fileName = add_param;
                    String folder = Environment.getExternalStorageDirectory() + File.separator + "serverDownload/";

                    File directory = new File(folder);

                    if (!directory.exists()) {
                        directory.mkdirs();
                    }

                    // Output stream to write file

                    OutputStream output = new FileOutputStream(folder + fileName +".txt");
                    int bytesRead = -1;
                    byte[] buffer = new byte[1024];
                    while ((bytesRead = input.read(buffer)) != -1) {
                        output.write(buffer, 0, bytesRead);
                    }

                    output.close();
                    input.close();
                    //donwload file end
                    */
                }

                /*if(responseCode == HttpURLConnection.HTTP_OK){
                    Log.v("connectTryOK", "HTTP OK");
                }*/

                //FIXME check if connecting

                return null;
            } catch (MalformedURLException e) {
                Log.d("connectTryOk", "We Failed");
                e.printStackTrace();
                 //return "Exception: "+e.getMessage();
                return null;
            } catch (IOException e) {
                Log.d("connectTryOk", "We Failed");
                e.printStackTrace();
               // return "Exception: "+e.getMessage();
                return null;
            }

        }
    }
}

