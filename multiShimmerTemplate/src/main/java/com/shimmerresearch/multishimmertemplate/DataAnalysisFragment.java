package com.shimmerresearch.multishimmertemplate;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import pl.flex_it.androidplot.XYSeriesShimmer;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidplot.Plot;
import com.androidplot.ui.AnchorPosition;
import com.androidplot.ui.DynamicTableModel;
import com.androidplot.ui.SizeLayoutType;
import com.androidplot.ui.SizeMetrics;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.XLayoutStyle;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYStepMode;
import com.androidplot.xy.YLayoutStyle;
import com.google.common.collect.BiMap;
import com.shimmerresearch.adapters.CheckboxListAdapter;
import com.shimmerresearch.adapters.ListViewFragmentAdapter;
import com.shimmerresearch.android.Shimmer;
import com.shimmerresearch.database.DatabaseHandler;
import com.shimmerresearch.database.ShimmerConfiguration;
import com.shimmerresearch.driver.FormatCluster;
import com.shimmerresearch.driver.ObjectCluster;
import com.shimmerresearch.driver.ShimmerObject;
import com.shimmerresearch.driver.ShimmerVerDetails;
import com.shimmerresearch.service.MultiShimmerTemplateService;


public class DataAnalysisFragment extends Fragment {

    public static MultiShimmerTemplateService mService;
    public View rootView = null;

    public static Activity mActivity;

    //MY VARIABLES
    public String[] dataTypes = new String[5];
    public String[] algorithms = new String[5];
    private ListView dv,av;

    Button setAnl;
    Button bck;

    public DataAnalysisFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().invalidateOptionsMenu();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_data_analysis, container, false);

        this.mService = ((MainActivity) getActivity()).mService;

        dataTypes[0] = "Integer";
        dataTypes[1] = "String";
        dataTypes[2] = "Boolean";
        dataTypes[3] = "Shimmer";
        dataTypes[4] = "Accelerometer";

        algorithms[0] = "Filter";
        algorithms[1] = "High Pass";
        algorithms[2] = "Low Pass";
        algorithms[3] = "Fall Detection";
        algorithms[4] = "Driving";

        dv = (ListView) rootView.findViewById(R.id.dataList);
        av = (ListView) rootView.findViewById(R.id.algoList);

        ArrayAdapter<String> arrayAdapterDT = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.data_analysis_textview,dataTypes);
        ArrayAdapter<String> arrayAdapterAL = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.data_analysis_textview,algorithms);

        dv.setAdapter(arrayAdapterDT);
        av.setAdapter(arrayAdapterAL);

        setAnl = (Button) rootView.findViewById(R.id.Set_Analysis);
        setAnl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //RUN CODE
            }
        });

        bck = (Button) rootView.findViewById(R.id.dataAnalysisBack);
        bck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
                Fragment fragment = new SensorProfile();
                FragmentManager fragmentManager = getFragmentManager();
                String tag = "Devices";
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment, tag).commit();
                getActivity().getActionBar().setTitle(tag); //set the title of the window
            }
        });

        return rootView;
    }

    public void setAnalysis(String BluetoothAddress, HashMap<String, String[]> sensors) {

    }

    public void deleteAnalysis() {

    }
}