package com.shimmerresearch.multishimmertemplate;

/**
 * Created by Hunter Reilly on 4/16/2018.
 */

public class User {
    String email, password;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public boolean check_cred(String email_in, String password_in){
        return(email.equals(email_in) && password.equals(password_in));
    }
}
