//Created 2-9-18

package com.shimmerresearch.multishimmertemplate;


import android.app.Activity;


import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.renderscript.Sampler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

/*
    test email: te@test.com
         pass : 12345
 */

public class LoginActivity extends Activity {
    Button lgn;
    Button skp; //FIXME Delete Later
    EditText email_textbox, password_textbox;
    String email_input, password_input;

    //NEW CODE
    User logged_user;
    UserLocalStore user_local_storage;

    String NAME=null, password_db =null, email_db =null;
    String db_verify = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        skp = (Button) findViewById(R.id.skip); //FIXME Delete Later
        email_textbox = (EditText) findViewById(R.id.editText);
        password_textbox = (EditText) findViewById(R.id.editText2);

        //NEW CODE
        user_local_storage = new UserLocalStore(this);
        /*
        //not sure what this is for?
        if(user_local_storage.getUserLoggedIn())
        {
            Intent activityIntent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(activityIntent);
            finish();
        }
        */

        //FIXME Delete Later
        skp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityIntent = new Intent(LoginActivity.this, MainActivity.class);
                activityIntent.putExtra("email input", "asdf@yahoo.com");
                startActivity(activityIntent);
                Toast.makeText(getApplicationContext(), "Redirecting...", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
        //FIXME Delete later
    }

    //function linked to login button
    public void main_login (View v){
        email_input = email_textbox.getText().toString();
        password_input = password_textbox.getText().toString();
        //check for internet connection
        if(!isNetworkAvailable()) {
            //check pre-existing user information
            Toast.makeText(getApplicationContext(), "No Internet Connection, Checking Local Storage", Toast.LENGTH_SHORT).show();
            logged_user = user_local_storage.getLoggedInUser();
            if(logged_user.check_cred(email_input,password_input)) {
                //password and email match what has been previously verified
                Intent activityIntent = new Intent(LoginActivity.this, MainActivity.class);
                activityIntent.putExtra("email input", email_input);
                startActivity(activityIntent);
                Toast.makeText(getApplicationContext(), "Redirecting...", Toast.LENGTH_SHORT).show();
                finish();
            }
            else{
                Toast.makeText(getApplicationContext(), "Incorrect email or password", Toast.LENGTH_SHORT).show();
            }
        }
        else{
            //check server for login credentials
            BackGround b = new BackGround();
            b.execute(email_input, password_input);
        }
    }

    class BackGround extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String email_input = params[0];
            String password_input = params[1];
            String db_return ="";
            int tmp;

            try {
                String urlParams = "?email="+ email_input +"&password="+ password_input;
                URL url = new URL("https://bsncloud.csufresno.edu/hunter_test/loginapp.php" + urlParams);


                HttpsURLConnection uRLConnection = (HttpsURLConnection) url.openConnection();
                /*
                uRLConnection.setDoOutput(true);
                OutputStream os = uRLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();
                */

                InputStream is = uRLConnection.getInputStream();
                while((tmp=is.read())!=-1){
                    db_return += (char)tmp;
                }

                is.close();
                //uRLConnection.disconnect();

                return db_return;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String db_return) {
            String err=null;
            try {
                JSONObject root = new JSONObject(db_return);
                JSONObject user_data = root.getJSONObject("user_data");
                //NAME = user_data.getString("name"); //No Name variable passed in the server response...
                //password_db = user_data.getString("password");
                //email_db = user_data.getString("email");
                db_verify = user_data.getString("response");
            } catch (JSONException e) {
                e.printStackTrace();
                err = "Exception: "+e.getMessage();
            }

            //TEMPORARY CODE
                //email_db = "test";
                //password_db = "test2";
            //TEMPORARY CODE

            //updated logic - Joseph
            if(db_verify.equals("FALSE")){
                //email and password not found on server
                Toast.makeText(getApplicationContext(), "Incorrect email or password", Toast.LENGTH_SHORT).show();
            }
            else if(db_verify.equals("TRUE")){
                //email and password found and matched on server
                User newUser = new User(email_input, password_input);
                user_local_storage.storeUserData(newUser);
                user_local_storage.setUserLoggedIn(true);
                Toast.makeText(getApplicationContext(), "Redirecting...", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
            else
                Toast.makeText(getApplicationContext(), "No DB response", Toast.LENGTH_SHORT).show();



            //Hunter's logic/test?
            /*
            if(((email_input.equals("admin@admin.com")) && (password_input.equals("admin123"))))
            {
                Toast.makeText(getApplicationContext(), "Redirecting...", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
            else {
                if ((email_db == null) || (password_db == null)) {
                    Toast.makeText(getApplicationContext(), "Could not fetch email or password", Toast.LENGTH_SHORT).show();
                } else {
                    if (((email_db.equals(email_input)) && (password_db.equals(password_input)))) {
                        User newUser = new User(email_input, password_input);
                        user_local_storage.storeUserData(newUser);
                        user_local_storage.setUserLoggedIn(true);
                        Toast.makeText(getApplicationContext(), "Redirecting...", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    } else
                        Toast.makeText(getApplicationContext(), "Wrong Credentials", Toast.LENGTH_SHORT).show();
                }
            }
            */
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}

