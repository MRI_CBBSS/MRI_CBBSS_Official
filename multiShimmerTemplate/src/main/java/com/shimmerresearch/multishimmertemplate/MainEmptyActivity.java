package com.shimmerresearch.multishimmertemplate;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import javax.annotation.Nullable;


public class MainEmptyActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent activityIntent;

        // go straight to main if a token is stored
        if (1 == 2 /*This will be used for the login token in the future. If the token is still valid, it will skip the login activity*/) {
            activityIntent = new Intent(this, MainActivity.class);
            System.out.println("I made it here.");
        } else {
            activityIntent = new Intent(this, LoginActivity.class);
        }
        startActivity(activityIntent);
        //finish();
    }
}
