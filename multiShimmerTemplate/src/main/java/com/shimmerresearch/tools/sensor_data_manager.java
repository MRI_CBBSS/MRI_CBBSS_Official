package com.shimmerresearch.tools;

/**
 * Created by mingli-student on 6/15/2018.
 */

//for each signal, the data manager stores segments features and the most recent data from the buffer
//when the corresponding signal's buffer is full, the buffer is flushed, ans a features segment is added to the data manager
//also, the flushed data becomes the mLast_segment_trp (triplet format from sensor_data_buffer)
public class sensor_data_manager {

    int mSegment_buffer_size;
    int mSegment_position;
    String mName;
    boolean mFull;
    segment_features[] mSegments;
    sensor_data_buffer.data_return_triplet mLast_segment_trp;

    //initialize segment count to be stored with mSegment_buffer_size
    public sensor_data_manager(int bff_size, String name) {
        mSegment_buffer_size = bff_size;
        mName = name;
        mSegment_position = 0;
        mFull = false;
        mSegments = new segment_features[bff_size];
        mLast_segment_trp = null;
    }

    //receive a triplet from sensor_data_buffer, and calculate the features with segment_features
    //replace mLast_segment_trp with new trp
    //if the buffer is full, set mFull = true and reset mSegment_position
    public void add_segment(sensor_data_buffer.data_return_triplet trp) {
        segment_features tmp = new segment_features(trp);
        mSegments[mSegment_position] = tmp;
        mLast_segment_trp.samples = trp.samples.clone();
        mLast_segment_trp.times = trp.times.clone();
        mLast_segment_trp.size = trp.size;
        mSegment_position++;
        if (mSegment_position >= mSegment_buffer_size) {
            mFull = true;
            mSegment_position = 0;
        }
    }

    //returns last triplet received
    public sensor_data_buffer.data_return_triplet get_last_segment_trp() {return mLast_segment_trp;}

    //return last segment features
    public segment_features get_last_segment_features () {
        if(mSegment_position > 0)
            return mSegments[mSegment_position - 1];
        else if (mFull)
            return mSegments[mSegment_buffer_size - 1];
        else
            return null;
    }

    //return all segment features from [from, from+size]
    public segment_features[] get_segment_range (int from, int size) {
        if (from > mSegment_buffer_size || size > mSegment_buffer_size) return null; //bad arguments
        if ((from + size) > mSegment_buffer_size) size = mSegment_buffer_size - from;
        segment_features[] temp = new segment_features[size];
        for (int i = 0, j = from; i <= size+from; i++) {
            temp[i] = mSegments[j% mSegment_buffer_size];
            j++;
        }
        return temp;
    }
}
