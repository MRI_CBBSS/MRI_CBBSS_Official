package com.shimmerresearch.tools;

import com.shimmerresearch.multishimmertemplate.SensorProfile;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.pow;

/**
 * Created by mingli-student on 3/28/2018.
 */

public class EventAnalysis {
    //event naming
    public static final String FALLINGEVENT = "FALLINGEVENT";
    //testing
    public static final String ON = "ON";
    public static final String ON2= "ON2";
    public static final String O2N = "O2N";

    List<EventAnalysisAPI> mEventList;

    public EventAnalysis(List<SensorProfile.analysisInfo> events) {
        mEventList = new ArrayList<EventAnalysisAPI>();
        for (SensorProfile.analysisInfo eve : events) {
            if (eve.mName.equals(FALLINGEVENT))
                mEventList.add(new fallingEvent(eve.mFrequency));
            //else if (eve.equals(event2))
              //mEventList.add(new event2Class());
            //test
            else if (eve.mName.equals(ON))
                mEventList.add(new oN(eve.mFrequency));
            else if (eve.mName.equals(ON2))
                mEventList.add(new oN2(eve.mFrequency));
            else if (eve.mName.equals(O2N))
                mEventList.add(new o2N(eve.mFrequency));

        }
    }

    public boolean RemoveEvent(String event) {
        for( EventAnalysisAPI an : mEventList) {
            if (event.equals(an.mName)) {
                return (mEventList.remove(an));
            }
        }
        return false;
    }

    public boolean AddEvent(SensorProfile.analysisInfo event) {
        for( EventAnalysisAPI an : mEventList) {
            if (event.mName.equals(an.mName)) {
                return (true);
            }
        }
        if (event.mName.equals(FALLINGEVENT)) {
            mEventList.add(new fallingEvent(event.mFrequency));
            return true;
        }
        //else if (eve.equals(event2))
        //mEventList.add(new event2Class());
        return false;
    }

    public List<EventAnalysisAPI> getEventAnalysis(double[] data) {
        List<EventAnalysisAPI> trueEvents = new ArrayList<EventAnalysisAPI>();
        for (EventAnalysisAPI event : mEventList) {
            if (event.mFrequencyCounter == 0) {
                if (event.runEventAnalysis(data)) trueEvents.add(event);
                event.mFrequencyCounter = event.mFrequency;
            }
            else {
                event.mFrequencyCounter = event.mFrequencyCounter - 1;
            }
        }
        //if no event has been triggered, return an empty list
        return trueEvents;
    }

    public abstract class EventAnalysisAPI {
        public String mName;
        public String mEventTrueMsg;
        public String mFollowupAnalysis;
        public int mFrequency;
        public int mFrequencyCounter;

        public EventAnalysisAPI(String name, String eventTrueMsg, String followupAnalysis, int frequency) {
            mName = name;
            mEventTrueMsg = eventTrueMsg;
            mFollowupAnalysis = followupAnalysis;
            mFrequency = frequency;
            mFrequencyCounter = frequency;
        }

        abstract public boolean runEventAnalysis(double[] data);
    }

    public class fallingEvent extends EventAnalysisAPI {
        public fallingEvent(int frequency) {
            super(FALLINGEVENT, "User is falling. Beginning fall analysis.", "", frequency);
        }

        public boolean runEventAnalysis(double[] data) {
            int b = 0;
            for (int i = 0; i < data.length; i++) {
                if (data[i] > 0) b++;
            }
            return (b > 20);
        }
    }

    //add event2 which extends EventAnalysisAPI
    // initializes member variables with super() in constructor and implements runEventAnalysis function

    //testing events
    public class oN extends EventAnalysisAPI {
        public oN(int frequency) {
            super(ON, "", "", frequency);
        }

        public boolean runEventAnalysis(double[] data) {
            int b = 0;
            for (int i = 0; i < data.length; i++) {
                if (data[i] > 0) b++;
            }
            return false;
        }
    }

    public class oN2 extends EventAnalysisAPI {
        public oN2(int frequency) {
            super(ON2, "", "", frequency);
        }

        public boolean runEventAnalysis(double[] data) {
            int b = 0;
            for (int i = 0; i < data.length; i++) {
                for (int j = 0; j < data.length; j++)
                    if (data[i] > 0) b++;
            }
            return false;
        }
    }

    public class o2N extends EventAnalysisAPI {
        public o2N(int frequency) {
            super(O2N, "", "", frequency);
        }

        public boolean runEventAnalysis(double[] data) {
            int b = 0;
            for (int i = 0; i < pow(2, data.length); i++) {
                if (data[0] > 0) b++;
            }
            return false;
        }
    }

}
