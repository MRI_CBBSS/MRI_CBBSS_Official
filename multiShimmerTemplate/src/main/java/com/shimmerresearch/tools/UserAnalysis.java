package com.shimmerresearch.tools;

import com.shimmerresearch.multishimmertemplate.SensorProfile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mingli-student on 3/28/2018.
 */

public class UserAnalysis {
    //analysis naming
    public static final String DISTANCE = "DISTANCE";

    List<UserAnalysisAPI> mAnalysisList;

    public UserAnalysis(List<SensorProfile.analysisInfo> analysisL) {
        mAnalysisList = new ArrayList<UserAnalysisAPI>();
        for (SensorProfile.analysisInfo analysis : analysisL) {
            if (analysis.mName.equals(DISTANCE))
                mAnalysisList.add(new distance(analysis.mFrequency));
            //else if (analysis.equals(Analysis2))
            //mAnalysisList.add(new Analysis2Class());
        }
    }

    public boolean RemoveAnalysis(String analysis) {
        for( UserAnalysisAPI an : mAnalysisList) {
            if (analysis.equals(an.mName)) {
                return (mAnalysisList.remove(an));
            }
        }
        return false;
    }

    public boolean AddAnalysis(SensorProfile.analysisInfo analysis) {
        for( UserAnalysisAPI an : mAnalysisList) {
            if (analysis.mName.equals(an.mName)) {
                return (true);
            }
        }
        if (analysis.mName.equals(DISTANCE)) {
            mAnalysisList.add(new distance(analysis.mFrequency));
            return true;
        }
        //else if (eve.equals(event2))
        //mEventList.add(new event2Class());
        return false;
    }

    public List<UserAnalysisAPI> getUserAnalysis(double[] data) {
        List<UserAnalysisAPI> analysisL = new ArrayList<UserAnalysisAPI>();
        for (UserAnalysisAPI analysis : mAnalysisList) {
            if (analysis.mFrequencyCount == 0) {
                analysis.runUserAnalysis(data);
                analysisL.add(analysis);
                analysis.mFrequencyCount = analysis.mFrequency;
            }
            else {
                analysis.mFrequencyCount = analysis.mFrequencyCount - 1;
            }
        }
        //results are stored in the class under mAnalysisResults
        return analysisL;
    }

    public abstract class UserAnalysisAPI {
        public String mName;
        public String mResultName;
        public String mResultUnits;
        public double mResults;
        public int mFrequency;
        public int mFrequencyCount;

        public UserAnalysisAPI(String name, String resultName, String resultUnits, int frequency) {
            mName = name;
            mResultName = resultName;
            mResultUnits = resultUnits;
            mResults = 0.0;
            mFrequency = frequency;
            mFrequencyCount = frequency;
        }

        abstract public void runUserAnalysis(double[] data);
    }

    public class distance extends UserAnalysisAPI {
        public distance(int frequency) {
            super(DISTANCE, "User is falling. Beginning fall analysis.", "", frequency);
        }

        public void runUserAnalysis(double[] data) {
            double dist = 0.0;
            for (int i = 0; i < data.length; i++) {
                if (data[i] > 0) dist++;
            }
            mResults = dist;
        }
    }

    //add Analysis2 which extends UserAnalysisAPI
    // initializes member variables with super() in constructor and implements runUserAnalysis function

}
