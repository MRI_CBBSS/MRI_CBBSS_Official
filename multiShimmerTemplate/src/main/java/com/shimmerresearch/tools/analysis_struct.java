package com.shimmerresearch.tools;

import java.util.Map;

/**
 * Created by mingli-student on 6/15/2018.
 */

public abstract class analysis_struct {

    String[] mSignals;
    String   mBT;
    String   mInput_type;
    String   mAnalysis_name;
    int      mPrioirity;
    int      mFrequency;
    long      mExecutionTime;
    int      mRelativeDeadline;
    boolean  mReady;


    //state information
    int mCount_for_round;

    public analysis_struct(String[] signals, int prioirity, int frequency, String BT) {
        mSignals = signals.clone();
        mPrioirity = prioirity;
        mFrequency = frequency;
        mBT         = BT;
    }

    abstract public void run_analysis(Map<String, sensor_data_manager> data_manager) ;

}
