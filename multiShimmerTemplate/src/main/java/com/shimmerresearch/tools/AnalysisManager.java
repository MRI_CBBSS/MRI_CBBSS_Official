package com.shimmerresearch.tools;

/**
 * Created by mingli-student on 6/15/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.util.Pair;

import com.shimmerresearch.multishimmertemplate.SensorProfile;
import com.shimmerresearch.multishimmertemplate.SensorProfile.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AnalysisManager {

    List<analysis_struct> mAnalysis_list_high;
    List<analysis_struct> mAnalysis_list_medium;
    List<analysis_struct> mAnalysis_list_low;
    Context mActivity_context;
    boolean mBudget_first_time;
    List<priority_index>[] mAnalysis_queue;
    int mBudget;
    int mBlock_time_seconds;
    int mBattery_per_block;
    int mBudget_partition_count;
    Calendar mStart_time;
    long mStart_time_ms;
    long mTime_from_start_ms;
    int mRounds_left;
    long mRound_time_ms;
    //long mTime_left_ms;
    float mPrev_battery_percent;
    float mExpected_battery_change_percent;
    float mCurrent_battery_percent;


    //new variables
    public int Bt_period;
    public long Bt_time;
    public boolean Bt_ready;
    public long Bt_relative_deadline;
    public long current_time;
    List<analysis_struct> analysis_list_1;
    List<analysis_struct> analysis_list_2;
    List<analysis_struct> analysis_list_3;

    //populates mAnalysis_queue
    public class priority_index {
        public int mPrioirty, mIndex;
        public priority_index(int p, int i) {
            mPrioirty = p; mIndex = i;
        }
    }

    public AnalysisManager(Context ctx){
        mStart_time = Calendar.getInstance();
        mStart_time_ms = System.currentTimeMillis();
        mActivity_context = ctx;
        getBattData(ctx);
        mRounds_left = (int) (mTime_from_start_ms / mRound_time_ms);
        mBudget_partition_count = 10; //default at 10 partitions per round

    }

    public void run_analysis() {

        Pair<Integer, Integer> scheduled_analysis = EDF_scheduler();
        long start_time = System.currentTimeMillis();
        if (scheduled_analysis.first == 1) {
            analysis_list_1.get(scheduled_analysis.second).run_analysis(SensorProfile.mData_managers[0]); //only one sensor for now **********use mData_managers[index of sensor for analysis]
        } else if (scheduled_analysis.first == 2) {
            analysis_list_2.get(scheduled_analysis.second).run_analysis(SensorProfile.mData_managers[0]);
        }
        else if (scheduled_analysis.first == 3) {
            analysis_list_3.get(scheduled_analysis.second).run_analysis(SensorProfile.mData_managers[0]);
        }
        long end_time = System.currentTimeMillis();

        long execution_time = end_time - start_time;
        //update execution time to worst case
        if (scheduled_analysis.first == 1) {
            if (analysis_list_1.get(scheduled_analysis.second).mExecutionTime < execution_time)
                analysis_list_1.get(scheduled_analysis.second).mExecutionTime = execution_time;
        } else if (scheduled_analysis.first == 2) {
            if (analysis_list_2.get(scheduled_analysis.second).mExecutionTime < execution_time)
                analysis_list_2.get(scheduled_analysis.second).mExecutionTime = execution_time;
        }
        else if (scheduled_analysis.first == 3) {
            if (analysis_list_3.get(scheduled_analysis.second).mExecutionTime < execution_time)
                analysis_list_3.get(scheduled_analysis.second).mExecutionTime = execution_time;
        }
        update_deadlines(scheduled_analysis);
    }

    //returns (List number, List Index) of analysis to be scheduled
    public Pair<Integer, Integer> EDF_scheduler() {
        Pair<Integer, Integer> scheduled_analysis = new Pair(-1,0);
        for(int i = 0; i < analysis_list_1.size(); i++) {
            if (scheduled_analysis.first == -1 && analysis_list_1.get(i).mReady) {
                scheduled_analysis = new Pair<>(0,i);
            }
            if (analysis_list_1.get(i).mReady && analysis_list_1.get(i).mRelativeDeadline < analysis_list_1.get(0).mRelativeDeadline) {
                scheduled_analysis = new Pair<>(0,i);
            }
        }
        if (scheduled_analysis.first != -1) return scheduled_analysis;

        for(int i = 0; i < analysis_list_2.size(); i++) {
            if (scheduled_analysis.first == -1 && analysis_list_2.get(i).mReady) {
                scheduled_analysis = new Pair<>(0,i);
            }
            if (analysis_list_2.get(i).mReady && analysis_list_2.get(i).mRelativeDeadline < analysis_list_2.get(0).mRelativeDeadline) {
                scheduled_analysis = new Pair<>(0,i);
            }
        }
        if (scheduled_analysis.first != -1) return scheduled_analysis;

        for(int i = 0; i < analysis_list_3.size(); i++) {
            if (scheduled_analysis.first == -1 && analysis_list_3.get(i).mReady) {
                scheduled_analysis = new Pair<>(0,i);
            }
            if (analysis_list_3.get(i).mReady && analysis_list_3.get(i).mRelativeDeadline < analysis_list_3.get(0).mRelativeDeadline) {
                scheduled_analysis = new Pair<>(0,i);
            }
        }

        return scheduled_analysis;
    }

    public void update_deadlines(Pair<Integer, Integer> scheduled_analysis) {

        current_time = System.currentTimeMillis();

        //update relative deadlines for each analysis
        for(int i = 0; i < analysis_list_1.size(); i++) {
            if (scheduled_analysis.first == 1 && scheduled_analysis.second == i) {
                if (analysis_list_1.get(i).mRelativeDeadline > current_time) {
                    analysis_list_1.get(i).mReady = false;
                }
                else {
                    while (analysis_list_1.get(i).mRelativeDeadline < current_time) {
                        analysis_list_1.get(i).mRelativeDeadline += analysis_list_1.get(i).mFrequency;
                    }
                }
            }
            else if (analysis_list_1.get(i).mRelativeDeadline < current_time) {
                while (analysis_list_1.get(i).mRelativeDeadline < current_time) {
                    analysis_list_1.get(i).mRelativeDeadline += analysis_list_1.get(i).mFrequency;
                }
                if(!analysis_list_1.get(i).mReady) analysis_list_1.get(i).mReady = true;
            }
        }
        for(int i = 0; i < analysis_list_2.size(); i++) {
            if (scheduled_analysis.first == 2 && scheduled_analysis.second == i) {
                if (analysis_list_2.get(i).mRelativeDeadline > current_time) {
                    analysis_list_2.get(i).mReady = false;
                }
                else {
                    while (analysis_list_2.get(i).mRelativeDeadline < current_time) {
                        analysis_list_2.get(i).mRelativeDeadline += analysis_list_2.get(i).mFrequency;
                    }
                }
            }
            else if (analysis_list_2.get(i).mRelativeDeadline < current_time) {
                while (analysis_list_2.get(i).mRelativeDeadline < current_time) {
                    analysis_list_2.get(i).mRelativeDeadline += analysis_list_2.get(i).mFrequency;
                }
                if(!analysis_list_2.get(i).mReady) analysis_list_2.get(i).mReady = true;
            }
        }
        for(int i = 0; i < analysis_list_3.size(); i++) {
            if (scheduled_analysis.first == 1 && scheduled_analysis.second == i) {
                if (analysis_list_3.get(i).mRelativeDeadline > current_time) {
                    analysis_list_3.get(i).mReady = false;
                }
                else {
                    while (analysis_list_3.get(i).mRelativeDeadline < current_time) {
                        analysis_list_3.get(i).mRelativeDeadline += analysis_list_3.get(i).mFrequency;
                    }
                }
            }
            else if (analysis_list_3.get(i).mRelativeDeadline < current_time) {
                while (analysis_list_3.get(i).mRelativeDeadline < current_time) {
                    analysis_list_3.get(i).mRelativeDeadline += analysis_list_3.get(i).mFrequency;
                }
                if(!analysis_list_3.get(i).mReady) analysis_list_3.get(i).mReady = true;
            }
        }
    }

    public void add_analysis_to_manager(analysisInfo analysis_info, String[] signals) {
        //find the right analysis based on the name
        if(analysis_info.mName.equals("A1")) {
            //check if mAnalysis_list_*priority* contains A1, if not
            //add_analysis_to_list(new A1(...))
        }
        else if(analysis_info.mName.equals("A2")) {
            //check if mAnalysis_list_*priority* contains A1, if not
            //add_analysis_to_list(new A1(...))
        }
        //.
        //.
        //.

    }

    //insert new analysis structure at the end of its respective priority list
    public void add_analysis_to_list(analysis_struct anal_struct) {
        if(anal_struct.mPrioirity == 1) analysis_list_1.add(anal_struct);
        else if(anal_struct.mPrioirity == 2) analysis_list_2.add(anal_struct);
        else if(anal_struct.mPrioirity == 3) analysis_list_3.add(anal_struct);
    }

    /*
    void budget_calculation(){
        //Calendar temp_time = Calendar.getInstance();
        //long temp_time_ms = System.currentTimeMillis();
        //long diff_ms = temp_time_ms - mStart_time_ms;
        //long time_left_ms = mTime_from_start_ms - diff_ms;
        mPrev_battery_percent = mCurrent_battery_percent;
        getBattData(mActivity_context);

        if (mBudget_first_time) {
            //calculate budget based
            mBudget = (int)mCurrent_battery_percent / (mRounds_left * mBattery_per_block);
            //how much battery is allocated to each round
            mExpected_battery_change_percent = mCurrent_battery_percent/(float)mRounds_left;
            mBudget_first_time = false;
        }
        else {
            //check predicted battery loss and correct budget if necessary
            float batt_change = mPrev_battery_percent - mCurrent_battery_percent;
            //Adjustments applied to the budget here

            //Simple adjustment
            if(batt_change > mExpected_battery_change_percent)  {
                mBudget = mBudget - 5;
            }
            else if (batt_change < mExpected_battery_change_percent) {
                mBudget = mBudget + 5;
            }

            //one round has passed
            mRounds_left -= 1;
        }

    }
    */
    /*
    void schedule_analysis() {

        int budget = mBudget;
        mAnalysis_queue = new List[mBudget_partition_count];

        //set size of partitions
        int[] partition_sizes = new int[mBudget_partition_count];
        for(int i = 0; i < mBudget_partition_count; i++) {
            mAnalysis_queue[i] = new ArrayList<>();
            if (i < (budget % mBudget_partition_count))
                partition_sizes[i] = budget / mBudget_partition_count + 1;
            else
                partition_sizes[i] = budget / mBudget_partition_count;
        }

        //analysis_queue (priority: 0 low, 1 medium, 2 high; index: i index of respective analysis_list)
        //fair approach
        //start with high priority
        for(int intensity = 0; intensity < 3; intensity++) {
            for (int i = 0; i < mAnalysis_list_high.size(); i++) {
                if(mAnalysis_list_high.get(i).mIntensity >= intensity) {
                    int p = 0;
                    while(p < mBudget_partition_count){
                        if(partition_sizes[p] >= mAnalysis_queue[p].size()){
                            mAnalysis_queue[p].add(new priority_index(2,i));
                            p += mBudget_partition_count/mAnalysis_list_high.get(i).mFrequency;
                        }
                        else
                            p++;
                    }
                }
            }
        }
        for(int intensity = 0; intensity < 3; intensity++) {
            for (int i = 0; i < mAnalysis_list_medium.size(); i++) {
                if(mAnalysis_list_high.get(i).mIntensity >= intensity) {
                    int p = 0;
                    while(p < mBudget_partition_count){
                        if(partition_sizes[p] >= mAnalysis_queue[p].size()){
                            mAnalysis_queue[p].add(new priority_index(1,i));
                            p += mBudget_partition_count/mAnalysis_list_medium.get(i).mFrequency;
                        }
                        else
                            p++;
                    }
                }
            }
        }
        for(int intensity = 0; intensity < 3; intensity++) {
            for (int i = 0; i < mAnalysis_list_low.size(); i++) {
                if(mAnalysis_list_high.get(i).mIntensity >= intensity) {
                    int p = 0;
                    while(p < mBudget_partition_count){
                        if(partition_sizes[p] >= mAnalysis_queue[p].size()){
                            mAnalysis_queue[p].add(new priority_index(0,i));
                            p += mBudget_partition_count/mAnalysis_list_low.get(i).mFrequency;
                        }
                        else
                            p++;
                    }
                }
            }
        }

    }
    */

    /*
     * helper functions for AnalysisManager class
     */

    public boolean getBattData(Context context)
    {
        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);

        Intent batteryStatus = context.registerReceiver(null, filter);

        if(batteryStatus!=null)
        {
            int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1 );
            // int en = BatteryManager.BATTERY_PROPERTY_ENERGY_COUNTER;
            // int cr = BatteryManager.BATTERY_PROPERTY_CURRENT_AVERAGE;
            // int crn = BatteryManager.BATTERY_PROPERTY_CURRENT_NOW;
            float batteryPct = level/(float)scale;

            if(batteryPct<mCurrent_battery_percent){
                mCurrent_battery_percent = batteryPct;
                return true;
            }
            //return (batteryPct*100);
            //return(crn);
        }
        return false;
        //return 0;
    }

}
