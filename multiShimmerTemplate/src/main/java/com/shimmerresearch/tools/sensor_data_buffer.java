package com.shimmerresearch.tools;


/**
 * Created by mingli-student on 6/15/2018.
 */

//**********************************************************************************************
//***** Buffer class to store most recent X data points for analysis algorithms
//**********************************************************************************************

//buffers for each sensor type created upon receiving an object cluster

public class sensor_data_buffer {

    int          BUFFSIZE;               //size of buffer, corresponding to size of one segment
    String       mName;                  //sensor name
    double[]     mData_samples;          //stores data values from the sensors
    double[]     mSample_times;          //stores time values corresponding to data sample
    int          mNext_open;             //marks the nextAvaliable poisition in the buffer
    public String       mFormat;
    String       mUnits;
    Boolean      mFull = false;

    //triplet (samples[], times[], size)
    public class data_return_triplet {
        public double[] samples;
        public double[] times;
        public int      size;

        public data_return_triplet(double[] fst, double[] snd, int thrd) {
            samples = fst;
            times = snd;
            size = thrd;
        }
    }

    /**
     * Construcor for sensorBuffer
     * initialize array for data storage
     * @param name sensor name specified by the Shimmer 3 manual
     * @param format
     * @param units
     */
    public sensor_data_buffer(String name, String format, String units, int size) {

        BUFFSIZE = size;
        mName   = name;
        mFormat = format;
        mUnits  = units;
        mNext_open = 0;
        mData_samples = new double[BUFFSIZE];
        mSample_times = new double[BUFFSIZE];
    }

    /**
     * function addData called in the ProcessData implementation when a new data message from the Shimmer has been received
     * checks to make sure the data is for the appropriate sensor and format
     * @param name
     * @param data
     * @param timeStamp
     */
    //add a sample to the buffer -> from handler in SensorProfile
    //check names are equal to verify it is the right signal
    //if the buffer is full -> return true so that the buffer can be flushed in the SensorProfile
    public boolean addData(String name, double data, double timeStamp){

        if (mName.equals(name)){
            mData_samples[mNext_open % BUFFSIZE] = data;
            mSample_times[mNext_open % BUFFSIZE] = timeStamp;

            if (mNext_open == BUFFSIZE) {
                mFull = true;
                mNext_open = (mNext_open + 1) % BUFFSIZE;
                return true;
            }
            mNext_open = (mNext_open + 1) % BUFFSIZE;
        }
        return false;
    }

    //returns data in the form of a triplet -> class definition at the top of file
    //fills samples and time, if buffer is not full, the end of the arrays will be zeros
    //clear samples and times in the buffer, reset mNext_open counter, and set mFull to false
    public data_return_triplet flushData(String name){
        if (mName == name) {
            double[] temp_samples = new double[BUFFSIZE];
            double[] temp_times = new double[BUFFSIZE];
            int size = mNext_open;
            for (int i = 0; i < BUFFSIZE; i++) temp_samples[i] = mData_samples[i];
            for (int i = 0; i < BUFFSIZE; i++) temp_times[i] = mSample_times[i];
            mNext_open = 0;
            mFull = false;
            mData_samples = new double[BUFFSIZE];
            mSample_times = new double[BUFFSIZE];

            return new data_return_triplet(temp_samples,temp_times,size);
        }

        return null;
    }

    /**
     * function getDataRange returns an array of data from a specified point of a specified position
     * @param name
     * @param from
     * @param size
     * @return
     */
    //returns a subset of the data from, [from, from+size]
    public double[] getDataRange(String name, int from, int size) {
        if (size > BUFFSIZE || from > BUFFSIZE) return null; //not valid inputs
        double[] temp = new double[size];
        if (mName.equals(name)){
            if((from + size) > BUFFSIZE) size = BUFFSIZE - from; //if size is too big, reduce it

            for (int i = 0, j = from; i <= from + size; i++){
                temp[i] = mData_samples[j%BUFFSIZE];
                j = j + 1 % BUFFSIZE;
            }
        }
        return temp;
    }

    /**
     * function getTimeRange returns an array of times from a specified point of a specified position
     * @param name
     * @param from
     * @param size
     * @return
     */
    //same as getDataRange()
    public double[] getTimeRange(String name, int from, int size) {
        if (size > BUFFSIZE || from > BUFFSIZE) return null; //not valid inputs
        double[] temp = new double[size];
        if (mName.equals(name)){
            if((from + size) > BUFFSIZE) size = BUFFSIZE - from;

            for (int i = 0, j = from; i <= size+from; i++){
                temp[i] = mSample_times[j%BUFFSIZE];
                j = j + 1 % BUFFSIZE;
            }
        }
        return temp;
    }

}
