package com.shimmerresearch.tools;

import java.util.Arrays;

/**
 * Created by mingli-student on 6/15/2018.
 */

//features stored for each segment of data instead of raw data (data summary)
//new features and their respective functions added here

public class segment_features {

    //statistics names
    public static final int MEAN = 0;
    public static final int VARIANCE = 1;
    public static final int STANDARD_DEVIATION = 2;
    public static final int MEDIAN = 3;
    public static final int IQR = 4;
    public static final int MAX = 5;
    public static final int MIN = 6;

    public int mStatistics_count = 7;

    public double[] mStatistic_features;

    public segment_features(sensor_data_buffer.data_return_triplet trp) {

        //extract statistical features
        mStatistic_features = new double[mStatistics_count];
        mStatistic_features[0] = Mean(trp.samples);
        mStatistic_features[1] = Variance(trp.samples);
        mStatistic_features[2] = Std(trp.samples);
        mStatistic_features[3] = Median(trp.samples);
        mStatistic_features[4] = IQR(trp.samples);
        mStatistic_features[5] = Max(trp.samples);
        mStatistic_features[6] = Min(trp.samples);


    }

    //*************************************************************
    //Statistics Implementations

    public static Double Mean(double[] data){
        Integer size = data.length;


        Double sum = 0.0;
        for(int i=0; i<=size-1; i++){
            sum = sum +data[i];
        }
        Double m = sum/size;

        return m;
    }

    public static Double Variance(double[] data){
        Integer size = data.length;

        Double mean =Mean(data);
        Double sumTemp =0.0;

        for(int i=0; i<=size-1; i++){
            sumTemp=sumTemp+ ((data[i]-mean)*(data[i]-mean));
        }

        Double var =sumTemp/size;
        return var;
    }


    public static Double Std(double[] data){
        return Math.sqrt(Variance(data));
    }

    public static Double Median(double[] data){
        Integer size = data.length;

        Arrays.sort(data);

        if (data.length % 2 == 0){
            return (double) (data[(size/2) - 1] + data[(size/2)]) / 2.0;
        }

        return data[(int) Math.floor(size/2)];
    }

    public static Double Quantiles(double[] data, Double dp){
        Integer size = data.length;

        Arrays.sort(data);
        Double[] quantiles=new Double[size];

        int index=-1;
        Double q=0.0;

        for(int i=0; i<=size-1; i++){
            quantiles[i]=(i+1-0.5)/size;
            if(i>0){
                if(dp>= quantiles[i-1] && dp<=quantiles[i]){
                    index=i;
                    break;
                }
            }
        }


        if(index !=-1){

            if(index<=size-1){
                q= data[index-1] + (data[index]-data[index-1])*((dp-quantiles[index-1])/(quantiles[index]-quantiles[index-1]));
            }

        }
        else if((1-dp) <=(dp-0)){
            q=data[size-1];
        }
        else{
            q=data[0];
        }


        return q;
    }

    public static Double IQR(double[] data){
        return Quantiles( data, .75)-Quantiles( data, .25);
    }

    public static Double Max(double[] data){
        return Quantiles( data, 1.0);
    }

    public static Double Min(double[] data){
        return Quantiles( data, 0.0);
    }
}
